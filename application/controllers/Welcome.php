<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        parent::__construct();     
		// $this->load->helper('language');
		// echo $this->session->userdata('site_lang');
		// if($this->session->userdata('site_lang') !=''){
		// 	$lan =$this->session->userdata('site_lang');
		// $this->lang->load('message',$lan);
		// }else{
		// 	$this->lang->load('message','english');
		// }
		
    }
 
	public function index()
	{

		if(isset($_GET['code'])){
			//echo $_GET['code'];
			// echo "string".$_GET['token'];
				$uri = 'https://api.instagram.com/oauth/access_token'; 
				$instagram_parameter = array(
					'client_id' => 'd385960b553c4b7aa0d3c508b5a6c710', 
					'client_secret' => '6471e4bedaa7428cbff28327388bbc04', 
					'grant_type' => 'authorization_code', 
					'redirect_uri' => 'http://localhost/chefdiner/index.php', 
					'code' => $_GET['code']
				);
	// echo '<pre>'; // preformatted view
	// print_r($instagram_parameter);
	$curl = curl_init('https://api.instagram.com/oauth/access_token');
	// curl_setopt($ch, CURLOPT_URL, $uri); // uri
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // RETURN RESULT true
	curl_setopt($curl, CURLOPT_POSTFIELDS, $instagram_parameter); // POST DATA
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // VERIFY SSL PEER false
	$result = curl_exec($curl);
	// echo "<pre>";
	
	curl_close($curl);
	$finalresult = json_decode($result); // execute curl

	$finalresult->access_token;

	$curl = curl_init('https://api.instagram.com/v1/users/self/media/recent/?access_token='.$finalresult->access_token);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // RETURN RESULT true
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // VERIFY SSL PEER false
	$mediaresult = curl_exec($curl);
	curl_close($curl);
	 	
	 $final = json_decode($mediaresult); // execute curl
	 //echo "<pre>";
 // print_r($final);
	 //echo '<pre>';
	$imagedata = $final->data;
	foreach ($final->data as $post) {
	//	print_r($post->carousel_media);
		// echo $value->images->standard_resolution->url;
		// print_r($value->images->standard_resolution);
	//	print_r($value->images['standard_resolution']);
	// echo '<img src="'.$value->images->standard_resolution->url.'" height="500px"
	// width="500px">';
		if(isset($post->carousel_media) && !empty($post->carousel_media)){
			
			foreach ($post->carousel_media as $key => $value) {
				//print_r($value);
				echo '<div class="caption"><a class="fancybox" href="'.$value->images->standard_resolution->url.'" data-fancybox-group="gallery" ><img src="'.$value->images->thumbnail->url .'"/></a></div>';
			}

		}

		echo '<a class="fancybox" href="'.$post->images->standard_resolution->url.'" data-fancybox-group="gallery" ><img src="'.$post->images->thumbnail->url .'"/></a>';
	}
	//echo'<div><iframe src="https://instagram.com/accounts/logout/" width="0" height="0">Logout</iframe></div>';
	//print_r($finalresult);
	// ecit directly the result
	//exit(print_r($result)); 

// 			$fields = array(
//            'client_id'     => 'd385960b553c4b7aa0d3c508b5a6c710',
//            'client_secret' => '6471e4bedaa7428cbff28327388bbc04',
//            'grant_type'    => 'authorization_code',
//            'redirect_uri'  => 'http://localhost/chefdiner/index.php',
//            'code'          => $_GET['code']
//     );
//     $url = 'https://api.instagram.com/oauth/access_token';

// 			//Get data from instagram api

// 		$_h = curl_init();
// curl_setopt($_h, CURLOPT_HEADER, 1);
// curl_setopt($_h, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($_h, CURLOPT_HTTPGET, 1);
// curl_setopt($_h, CURLOPT_URL, $url );
// curl_setopt($_h, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
// curl_setopt($_h, CURLOPT_SSL_VERIFYHOST, 0);
// curl_setopt($_h, CURLOPT_SSL_VERIFYPEER, 0);
// curl_setopt($_h, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
// curl_setopt($_h, CURLOPT_POSTFIELDS, $fields);
// var_dump(curl_exec($_h));
// var_dump(curl_getinfo($_h));
// var_dump(curl_error($_h)); 


		}else{
		$this->load->view('welcome_message');
	}
	}
	
	// function switchLang($language = "") {
        
 //        $language = ($language != "") ? $language : "english";
 //        $this->session->set_userdata('site_lang', $language);
        
 //        redirect($_SERVER['HTTP_REFERER']);
        
 //    }


	// function sendsms($number, $message_body, $return = '0') {
	function sendsms($return = '0') {


			$sender = 'SEDEMO'; // Need to change

			// $smsGatewayUrl = 'http://springedge.com';

			$smsGatewayUrl = 'https://instantalerts.co';
			
			$apikey = '69iq54a4m4s4ib0agg135o3y0yfbkbmbu'; // Need to change

			$textmessage='this is the test sms.';

			$textmessage = urlencode($textmessage);

			$api_element = '/api/web/send/';

			$mobileno = '7790910393';

			$api_params = $api_element.'?apikey='.$apikey.'&sender='.$sender.'&to='.$mobileno.'&message='.$textmessage;
			$smsgatewaydata = $smsGatewayUrl.$api_params;

			$url = $smsgatewaydata;

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POST, false);

			curl_setopt($ch, CURLOPT_URL, $url);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); $output = curl_exec($ch);

			curl_close($ch);

			if(!$output){ $output = file_get_contents($smsgatewaydata); }

			if($return == '1'){ return $output; }else{ echo "Sent"; }

}
}
