<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//load rest library
require APPPATH . '/libraries/REST_Controller.php';


class Apicontroller extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 *		http://example.com/service/index
	 * @see http://codeigniter.com/user_guide/general/urls.html
         * 
	 */
	/********Code developed by priyanka verma*****/

    public function __construct() {
        parent::__construct();
        $this->load->model('api_model');
        $this->load->library('encrypt');
		
        
    }


    /********Login API******/
    public function userLogin_post()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('useremail','User Email','required');
		$this->form_validation->set_rules('password','Password','required');
		//$this->form_validation->set_rules('deviceId','Device','required');
		
		if($this->form_validation->run() == FALSE){
			$responseArray = array('status'=>'0','message'=>validation_errors());
			$response = $this->generate_response($responseArray);
			$this->response($response);
		} else {
			//load library for encryption
			$this->load->library('encrypt');
			
			$authToken = $this->api_model->_generate_token();
			
			$data['email'] = $this->post('useremail');
			$data['password'] = $this->post('password');
			$data['deviceId'] = $this->post('deviceId');
			$data['deviceType'] = $this->post('deviceType');
			$data['authtoken'] = $authToken;
			$res = $this->api_model->userLogin($data);
		//	print_r($res);die;
			if(is_string($res) && $res == 'NA'){
				//echo "string";die;
				$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(121));
				$status = ResponseMessages::getStatusCodeMessage(200); //OK
			} elseif(is_string($res) && $res == 'WP'){
				//echo "string1";die;
				$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(105));
				$status = ResponseMessages::getStatusCodeMessage(118); //SERVER_ERROR;
			} elseif(is_array($res)){
				//echo "string2";die;
				$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'userData'=>$res);
				$status = ResponseMessages::getStatusCodeMessage(508);
			}else{
				//echo "string3";die;
				$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(102));
				$status =  ResponseMessages::getStatusCodeMessage(118);  //SERVER_ERROR;
			}
			
			$response = $this->generate_response($responseArray);
			$this->response($response,$status);
		
		}
	}

	/********Registeration API***********/


	 public function userRegister_post()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('firstname','User Email','required');
		$this->form_validation->set_rules('lastname','Password','required');
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('contact','Contact Number','required');
		/*******Generate refferal code*******/
		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$res = "";
		for ($i = 0; $i < 10; $i++) {
		    $res .= $chars[mt_rand(0, strlen($chars)-1)];
		}
		$language = $this->post('language');
		if($this->form_validation->run() == FALSE){
			$responseArray = array('status'=>0,'message'=>validation_errors());
			$response = $this->generate_response($responseArray);
			$this->response($response);
		} else {
			//load library for encryption
			$this->load->library('encrypt');
			
			$data['first_name'] = $this->post('firstname');
			$data['last_name'] = $this->post('lastname');
			$data['email'] = $this->post('email');
			$data['status'] = '1';
			$data['is_user'] = '1';
		    $data['referal_code'] = $res;
			$data['password'] = md5($this->post('password'));
			$data['phone_number'] = $this->post('contact');
			$data['language'] = $language;
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$res = $this->api_model->userRegister($data);
			
			if(is_string($res) && $res == 'NR'){
				$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(121));
				$status = ResponseMessages::getStatusCodeMessage(200); //OK
			}elseif(is_string($res) && $res == 'AE'){
				$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(116));
				$status = ResponseMessages::getStatusCodeMessage(118); //SERVER_ERROR;
			}elseif(is_array($res)){
				$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(506),'userData'=>$res);
				$status = ResponseMessages::getStatusCodeMessage(200); //OK
			}else{
				$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(102));
				$status = ResponseMessages::getStatusCodeMessage(118); //SERVER_ERROR;
			}
			
			$response = $this->generate_response($responseArray);
			$this->response($response,$status);
		
		}
	}


	

	/**********Become a chef**********/


	 public function becomeChef_post()
	{
			$picture='';
			$data['user_id'] = ($this->post('user_id')) ? $this->post('user_id') : '';
			$data['language'] = ($this->post('language')) ? $this->post('language') : '';
			$data['chef_fname'] = ($this->post('chef_fname')) ? $this->post('chef_fname') : '';
			$data['chef_lname'] = ($this->post('chef_lname')) ? $this->post('chef_lname') : '';
			$data['language_speak'] = ($this->post('language_speak')) ? $this->post('language_speak') : '';//send in comma separated format
			$data['phone_number'] = ($this->post('phone_number')) ? $this->post('phone_number') : '';
			$data['chef_country'] = ($this->post('chef_country')) ? $this->post('chef_country') : '';
			$data['chef_city'] = ($this->post('chef_city')) ? $this->post('chef_city') : '';
			$data['from_guest'] = ($this->post('from_guest')) ? $this->post('from_guest') : '';
			$data['upto_guest'] = ($this->post('upto_guest')) ? $this->post('upto_guest') : '';
			$data['kitchen_title'] = ($this->post('kitchen_title')) ? $this->post('kitchen_title') : '';
			$data['kitchen_descrition'] = ($this->post('kitchen_descrition')) ? $this->post('kitchen_descrition') : '';
			$data['service_type'] = ($this->post('service_type')) ? $this->post('service_type') : '';//send in comma separated format
			$data['oftencook'] = ($this->post('oftencook')) ? $this->post('oftencook') : '';
			$data['chef_latitude'] = ($this->post('chef_latitude')) ? $this->post('chef_latitude') : '';
			$data['chef_longitude'] = ($this->post('chef_longitude')) ? $this->post('chef_longitude') : '';
			
			$data['brunch'] = ($this->post('brunch')) ? $this->post('brunch') : '';
			$data['lunch'] = ($this->post('lunch')) ? $this->post('lunch') : '';
			$data['dinner'] = ($this->post('dinner')) ? $this->post('dinner') : '';
			$data['currency'] = ($this->post('currency')) ? $this->post('currency') : '';
			$data['cusines'] = ($this->post('cusines')) ? $this->post('cusines') : ''; //send in comma separated format
			$data['status'] = '1';
		/*****Upload profile pic*******/
			if(!empty($_FILES['fileToUpload']['name'])){
                            $config['upload_path'] = 'assets/chefprofile/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['fileToUpload']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('fileToUpload')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }
                            $data['profile_pic'] = $picture;
            }
            /*else{
                $picture = '';
                $data['profile_pic'] = $picture;
            }*/
          //  $data['profile_pic'] = $picture;
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');

			// echo '<pre>';
			// print_r($data);die;

			$becomeChef = $this->api_model->becomeChef($data);
			
	if(!empty($becomeChef)){
	$responseArray = array('status'=>'1','id'=>$becomeChef['id'],'message'=>ResponseMessages::getStatusCodeMessage(200));
	}else{
	$responseArray = array('status'=>'0','id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
	
	}

		
		$response = $this->generate_response($responseArray);
			$this->response($response);
	}

	/*****get country******/
	function country_get(){

		$key = 'qJB0rGtIn5UB1xG03efyCp';
		$country= $this->api_model->country();
		$encryptpss = $this->encrypt->encode($country, $key);
		
		if(!empty($country)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'country'=>$encryptpss);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(102));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
    }

    /*******get city on the basis of country*********/
	function city_get(){
		$countryid = $this->get('countryid') ?  $this->get('countryid') : 0;
		$getcity= $this->api_model->getcity($countryid);
		
		
		if(!empty($getcity)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'getcity'=>$getcity);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(102));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
    }

    /*******Get list of chef ********/

    function getChef_get(){
    	$latitude = $this->get('latitude') ?  $this->get('latitude') : 0;
    	$longitude = $this->get('longitude') ?  $this->get('longitude') : 0;

    	
 		$getfave = $this->api_model->getfave();
    	$getChef= $this->api_model->getChef($latitude,$longitude);
		   if(!empty($getfave)){
		   	foreach ($getfave as $key => $value) {
		    		$chefarr[] = $value->chef_id;
		    		$chefarrq=array_count_values($chefarr);
		    	}
		    }else{
		    	$chefarrq='';
		    }
//print_r($chefarrq);die;
		$getprice = $this->api_model->getprice();
    	if(!empty($getprice)){
		   	foreach ($getprice as $key => $value) {
		    		$getless[$value->id] = $value->price_per_person;
		    		
		    	}
		    }else{
		    	$getless='';
		    }
    	//print_r($getless);die;
if(!empty($getChef)){
 foreach ($getChef as $key => $value) {
	    	if($chefarrq !=''){
	    	if(array_key_exists($value->id, $chefarrq)){
			 	$getChef[$key]->favouritecount = $chefarrq[$value->id];
		    } else{
		    	$getChef[$key]->favouritecount =0;
		    }
	    	}
		   else{
		    	$getChef[$key]->favouritecount =0;
		    }

		    if($getless !=''){
	    	if(array_key_exists($value->id, $getless)){
			 	$getChef[$key]->price = $getless[$value->id];
		    } else{
		    	$getChef[$key]->price =0;
		    }
	    	}
		   else{
		    	$getChef[$key]->price =0;
		    }

		   
		    //$getchef[$key]->chefCuisine = $cusn;
	    }
}
	   

 // /print_r(array_values($cusn));
	   // print_r($cusn);die;
	
		if(!empty($getChef)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'getChef'=>$getChef);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
    }

    /*******Add menus********/
    function addMenu_post(){
    		$picture='';
    		$data['id'] = ($this->post('menu_id')) ? $this->post('menu_id') : '0';
			$data['menu_title'] = ($this->post('menu_title')) ? $this->post('menu_title') : '';
			$data['service_type'] = ($this->post('service_type')) ? $this->post('service_type') : '';//send in comma separated format
			$data['price_per_person'] = ($this->post('price_per_person')) ? $this->post('price_per_person') : '';
			$data['cusine'] = ($this->post('cusine')) ? $this->post('cusine') : ''; // comma separated format
			$data['subcusine'] = ($this->post('subcusine')) ? $this->post('subcusine') : ''; // comma separated format
			$data['user_id'] = ($this->post('user_id')) ? $this->post('user_id') : '';
			$data['currency'] = ($this->post('currency')) ? $this->post('currency') : '';
			$data['status'] = '1';
		/*****Upload profile pic*******/
			if(!empty($_FILES['fileToUpload']['name'])){
                            $config['upload_path'] = 'assets/menuImgae/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['fileToUpload']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('fileToUpload')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }
                            $data['menu_image'] = $picture;
            }
            /*else{
                $picture = '';
            }*/
            // $data['menu_image'] = $picture;
			

			$addMenu = $this->api_model->addMenu($data);
			
			if(!empty($addMenu)){
			$responseArray = array('status'=>'1','id'=>$addMenu['id'],'message'=>ResponseMessages::getStatusCodeMessage(200));
			}else{
			$responseArray = array('status'=>'0','id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
			}
				
			
		
		$response = $this->generate_response($responseArray);
			$this->response($response);
    }

    /******Add submenu or dishes******/
     function addSubMenu_post(){
     		$picture='';
    		$data['id'] = ($this->post('id')) ? $this->post('id') : '0';
			$data['menu_id'] = ($this->post('menu_id')) ? $this->post('menu_id') : '';
			// get menu title and price
			$getmenutitleprice = $this->api_model->getmenutitleprice($data['menu_id']);
			$data['menu_title'] = $getmenutitleprice[0]->menu_title;
			$data['menu_price'] = $getmenutitleprice[0]->price_per_person;
			$data['currency'] =  $getmenutitleprice[0]->currency;
			$data['user_id'] = ($this->post('user_id')) ? $this->post('user_id') : '';
			$data['dish_name'] = ($this->post('dish_name')) ? $this->post('dish_name') : '';
			$data['dish_category'] = ($this->post('dish_category')) ? $this->post('dish_category') : '';
			$data['status'] = '1';
		/*****Upload profile pic*******/
			if(!empty($_FILES['fileToUpload']['name'])){
                            $config['upload_path'] = 'assets/submenuImage/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['fileToUpload']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('fileToUpload')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }
                            $data['dish_image'] = $picture;
            }
            /*else{
                $picture = '';
            }*/
            // $data['dish_image'] = $picture;
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');

			$addSubMenu = $this->api_model->addSubMenu($data);
			
			if(!empty($addSubMenu)){
			$responseArray = array('status'=>'1', 'id'=>$addSubMenu['id'],'message'=>ResponseMessages::getStatusCodeMessage(200));
			}else{
			$responseArray = array('status'=>'0', 'id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
			}
				
			
		
		$response = $this->generate_response($responseArray);
			$this->response($response);
    }

    /*******get mrenu***/
    function getMenus_get(){
    	$userid = ($this->get('user_id')) ? $this->get('user_id') : '';
    	$getMenus= $this->api_model->getMenus($userid);

    	$getcounterdish = $this->api_model->getcounterdish();
    	// /print_r($getMenus);
		
		if(!empty($getcounterdish)){
		   	foreach ($getcounterdish as $key => $value) {
		    		$menuar[] = $value->menu_id;
		    		$menuarrq=array_count_values($menuar);
		    	}
		    }else{
		    	$menuarrq='';
		    }

		   foreach ($getMenus as $key => $value) {
	    	if($menuarrq !=''){
	    	if(array_key_exists($value->id, $menuarrq)){
			 	$getMenus[$key]->countsubmenu = $menuarrq[$value->id];
		    } else{
		    	$getMenus[$key]->countsubmenu =0;
		    }
	    	}
		   else{
		    	$getMenus[$key]->countsubmenu =0;
		    }
		}
		//print_r($menuarrq);die;
		if(!empty($getMenus)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'getMenus'=>$getMenus);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
    }

    /*******Get sub menu*****/
      function getSubMenus_get(){
      	$menuid = ($this->get('menu_id')) ? $this->get('menu_id') : '';
      //	echo $menuid;die;
    	$getSubMenus= $this->api_model->getSubMenus($menuid);
		
		
		if(!empty($getSubMenus)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'getSubMenus'=>$getSubMenus);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
    }


    /*********Delete menus*********/

    function deleteMenus_get(){
		$menuid = ($this->get('menu_id')) ? $this->get('menu_id') : '';
		$deleteMenus= $this->api_model->deleteMenus($menuid);

		if(!empty($deleteMenus)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}

	}
	/*********Delete sub menus*********/
	function deleteSubmenus_get(){
		$submenu = ($this->get('submenu_id')) ? $this->get('submenu_id') : '';

		$deleteSubmenus= $this->api_model->deleteSubmenus($submenu);

		if(!empty($deleteSubmenus)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
	}

	/*********Get user info**********/

	/*function getUserinfo_get(){
		$userid = ($this->get('user_id')) ? $this->get('user_id') : '';
      //	echo $menuid;die;
    	$getUserinfo= $this->api_model->getUserinfo($userid);
		
		
		if(!empty($getUserinfo)){
			$responseArray = array('status'=>1,'message'=>ResponseMessages::getStatusCodeMessage(200),'getUserinfo'=>$getUserinfo);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>0,'message'=>ResponseMessages::getStatusCodeMessage(507));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
	}*/

	/********Update user profile and if it is chef then also update chef profile values******/

	 /******Add submenu or dishes******/
     function UpdateuserProfile_post(){
     		$picture=''; 
    		$userid = ($this->post('id')) ? $this->post('id') : '0';
			$data['first_name'] = ($this->post('first_name')) ? $this->post('first_name') : '';
			$data['last_name'] = ($this->post('last_name')) ? $this->post('last_name') : '';
			$data['currency'] = ($this->post('currency')) ? $this->post('currency') : '';
		/*****Upload profile pic*******/
			if(!empty($_FILES['fileToUpload']['name'])){
				// $pictureupload = $this->uploadImage('fileToUpload');
				// $picture = $pictureupload['upload_data']['file_name'];
				// echo $picture;
				// print_r($pictureupload);die;
                            $config['upload_path'] = 'assets/chefprofile/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['fileToUpload']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('fileToUpload')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }
                            $data['profile_pic'] = $picture;
            }
            /*else{
                $picture = '';
            }*/
            // $data['profile_pic'] = $picture;
			$data['updated_at'] = date('Y-m-d H:i:s');

			$UpdateuserProfile = $this->api_model->UpdateuserProfile($data,$userid);

			//print_r($UpdateuserProfile);die;
			if(!empty($UpdateuserProfile) && $UpdateuserProfile['user'] == 'U'){
			//	echo "1";die;
			$responseArray = array( 'status'=>'1', 'id'=>$UpdateuserProfile['id'],'message'=>ResponseMessages::getStatusCodeMessage(509));
			}else if(!empty($UpdateuserProfile) && $UpdateuserProfile['user'] == 'UC'){
				//echo "2";die;
			$responseArray = array('status'=>'1', 'id'=>$UpdateuserProfile['id'],'message'=>ResponseMessages::getStatusCodeMessage(510));
			}else{
				//echo "3";die;
			$responseArray = array('status'=>'0' ,'id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
			}
				
			$response = $this->generate_response($responseArray);
			$this->response($response);
    }

    /**********Get chef *********/
    function getchefinfo_get(){
		$userid = ($this->get('user_id')) ? $this->get('user_id') : '';
      //	echo $menuid;die;
    	$getchefinfo= $this->api_model->getchefinfo($userid);
		
		
		if(!empty($getchefinfo)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'getchefinfo'=>$getchefinfo);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507)); //no records found
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
	}

	/**********Get user info *********/
    function getuserinfo_get(){
		$userid = ($this->get('user_id')) ? $this->get('user_id') : '';
      //	echo $menuid;die;
    	$getuserinfo= $this->api_model->getuserinfo($userid);
		
		
		if(!empty($getuserinfo)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'getuserinfo'=>$getuserinfo);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507)); //no records found
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
	}
	 /*******Add Permission********/
    function addPermission_post(){
    		$data['id'] = ($this->post('id')) ? $this->post('id') : '0';
			$data['user_id'] = ($this->post('user_id')) ? $this->post('user_id') : '';
			$data['email_pro'] = ($this->post('email_pro')) ? $this->post('email_pro') : '0';
			$data['email_msg'] = ($this->post('email_msg')) ? $this->post('email_msg') : '0';
			$data['sms_msg'] = ($this->post('sms_msg')) ? $this->post('sms_msg') : '0';
			$data['mobile_msg'] = ($this->post('mobile_msg')) ? $this->post('mobile_msg') : '0';
			$data['mobile_pro'] = ($this->post('mobile_pro')) ? $this->post('mobile_pro') : '0';
			$data['status'] = '1';
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');

			$addPermission = $this->api_model->addPermission($data);
			
			if(!empty($addPermission)){
			$responseArray = array('status'=>'1','id'=>$addPermission['id'],'message'=>ResponseMessages::getStatusCodeMessage(200));
			}else{
			$responseArray = array('status'=>'0','id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
			}
				
			
		
		$response = $this->generate_response($responseArray);
			$this->response($response);
    }

    /********Get Permission*******/
    function getPermission_get(){
    	$userid = ($this->get('user_id')) ? $this->get('user_id') : '';
      //	echo $menuid;die;
    	$getPermission= $this->api_model->getPermission($userid);
		
		
		if(!empty($getPermission)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'getPermission'=>$getPermission);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507)); //no records found
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
    }

    function uploadImage($filename){
				$config['upload_path'] = 'assets/kitchengallery/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				// $config['max_size'] = '100';
				// $config['max_width']  = '1024';
				// $config['max_height']  = '768';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload($filename)) {
				    $error = array('error' => $this->upload->display_errors());
				return false;
				// $this->load->view('upload_form', $error);
				} else {
				$data = array('upload_data' => $this->upload->data());
				return $data;
				//$this->load->view('upload_success', $data);
				}
    }


    /*******Kitchen Gallery**********/

  //   function KitchenGallery_post(){
  //   $picture1=$picture2=$picture3=$picture4=$picture5=$picture6=$picture7=$picture8=$picture9=$picture10='';
  //   $picture11=$picture12=$picture13=$picture14=$picture15=$picture16=$picture17=$picture18 =$picture19=$picture20='';
  //   		$data['id']=($this->post('id')) ? $this->post('id') : '';
  //   		$data['chef_id'] = ($this->post('chef_id')) ? $this->post('chef_id') : '0';
			
		// /*****Upload profile pic*******/
		
		// 	if (isset($_FILES['fileToUpload1']) && $_FILES['fileToUpload1']['name'] != ''){
		// 		/*******Upload image 1**********/
			    
		// 	    $pictureupload1 = $this->uploadImage('fileToUpload1');
		// 		$picture1 = $pictureupload1['upload_data']['file_name'];
		// 		$data['image1'] = $picture1;
		// 	}else if (isset($_FILES['fileToUpload2']) && $_FILES['fileToUpload2']['name'] != ''){
		// 		/*******Upload image 2**********/    
			    
		// 	    $pictureupload2 = $this->uploadImage('fileToUpload2');
		// 		$picture2 = $pictureupload2['upload_data']['file_name'];
		// 		$data['image2'] = $picture2;
		// 	}else if (isset($_FILES['fileToUpload3']) && $_FILES['fileToUpload3']['name'] != ''){
		// 		/*******Upload image 3**********/    
			    
		// 	    $pictureupload3 = $this->uploadImage('fileToUpload3');
		// 		$picture3 = $pictureupload3['upload_data']['file_name'];
		// 		$data['image3'] = $picture3;
		// 	}else if (isset($_FILES['fileToUpload4']) && $_FILES['fileToUpload4']['name'] != ''){
		// 		/*******Upload image 4**********/    
			    
		// 	    $pictureupload4 = $this->uploadImage('fileToUpload4');
		// 		$picture4 = $pictureupload4['upload_data']['file_name'];
		// 		$data['image4'] = $picture4;
		// 	}else if (isset($_FILES['fileToUpload5']) && $_FILES['fileToUpload5']['name'] != ''){
		// 		/*******Upload image 5**********/    
			    
		// 	    $pictureupload5 = $this->uploadImage('fileToUpload5');
		// 		$picture5 = $pictureupload5['upload_data']['file_name'];
		// 		$data['image5'] = $picture5;
		// 	}else if (isset($_FILES['fileToUpload6']) && $_FILES['fileToUpload6']['name'] != ''){
		// 		/*******Upload image 6**********/    
			    
		// 	    $pictureupload6 = $this->uploadImage('fileToUpload6');
		// 		$picture6 = $pictureupload6['upload_data']['file_name'];
		// 		$data['image6'] = $picture6;
		// 	}else if (isset($_FILES['fileToUpload7']) && $_FILES['fileToUpload7']['name'] != ''){
		// 		/*******Upload image 7**********/    
			    
		// 	    $pictureupload7 = $this->uploadImage('fileToUpload7');
		// 		$picture7 = $pictureupload7['upload_data']['file_name'];
		// 		$data['image7'] = $picture7;
		// 	}else if (isset($_FILES['fileToUpload8']) && $_FILES['fileToUpload8']['name'] != ''){
		// 		/*******Upload image 8**********/    
			    
		// 	    $pictureupload8 = $this->uploadImage('fileToUpload8');
		// 		$picture8 = $pictureupload8['upload_data']['file_name'];
		// 		$data['image8'] = $picture8;
		// 	}else if (isset($_FILES['fileToUpload9']) && $_FILES['fileToUpload9']['name'] != ''){
		// 		/*******Upload image 9**********/    
			    
		// 	    $pictureupload9 = $this->uploadImage('fileToUpload9');
		// 		$picture9 = $pictureupload9['upload_data']['file_name'];
		// 		$data['image9'] = $picture9;
		// 	}else if (isset($_FILES['fileToUpload10']) && $_FILES['fileToUpload10']['name'] != ''){
		// 		/*******Upload image 10**********/    
			    
		// 	    $pictureupload10 = $this->uploadImage('fileToUpload10');
		// 		$picture10 = $pictureupload10['upload_data']['file_name'];
		// 		$data['image10'] = $picture10;
		// 	}else if (isset($_FILES['fileToUpload11']) && $_FILES['fileToUpload11']['name'] != ''){
		// 		******Upload image 11*********
			    
		// 	    $pictureupload11 = $this->uploadImage('fileToUpload11');
		// 		$picture11 = $pictureupload11['upload_data']['file_name'];
		// 		$data['image11'] = $picture11;
		// 	}else if (isset($_FILES['fileToUpload12']) && $_FILES['fileToUpload12']['name'] != ''){
		// 		/*******Upload image 12**********/    
			   
		// 	    $pictureupload12 = $this->uploadImage('fileToUpload12');
		// 		$picture12 = $pictureupload12['upload_data']['file_name'];
		// 		$data['image12'] = $picture12;
		// 	}else if (isset($_FILES['fileToUpload13']) && $_FILES['fileToUpload13']['name'] != ''){
		// 		/*******Upload image 13**********/    
			    
		// 	    $pictureupload13 = $this->uploadImage('fileToUpload13');
		// 		$picture13 = $pictureupload13['upload_data']['file_name'];
		// 		$data['image13'] = $picture13;
		// 	}else if (isset($_FILES['fileToUpload14']) && $_FILES['fileToUpload14']['name'] != ''){
		// 		/*******Upload image 14**********/    
			    
		// 	    $pictureupload14 = $this->uploadImage('fileToUpload14');
		// 		$picture14 = $pictureupload14['upload_data']['file_name'];
		// 		$data['image14'] = $picture14;
		// 	}else if (isset($_FILES['fileToUpload15']) && $_FILES['fileToUpload15']['name'] != ''){
		// 		/*******Upload image 15**********/    
			    
		// 	    $pictureupload15 = $this->uploadImage('fileToUpload15');
		// 		$picture15 = $pictureupload15['upload_data']['file_name'];
		// 		$data['image15'] = $picture15;
		// 	}else if (isset($_FILES['fileToUpload16']) && $_FILES['fileToUpload16']['name'] != ''){
		// 		/*******Upload image 16**********/    
			    
		// 	    $pictureupload16 = $this->uploadImage('fileToUpload16');
		// 		$picture16 = $pictureupload16['upload_data']['file_name'];
		// 		$data['image16'] = $picture16;
		// 	}else if (isset($_FILES['fileToUpload17']) && $_FILES['fileToUpload17']['name'] != ''){
		// 		/*******Upload image 17**********/    
			    
		// 	    $pictureupload17 = $this->uploadImage('fileToUpload17');
		// 		$picture17 = $pictureupload17['upload_data']['file_name'];
		// 		$data['image17'] = $picture17;
		// 	}else if (isset($_FILES['fileToUpload18']) && $_FILES['fileToUpload18']['name'] != ''){
		// 		/*******Upload image 18**********/    
			    
		// 	    $pictureupload18 = $this->uploadImage('fileToUpload18');
		// 		$picture18 = $pictureupload18['upload_data']['file_name'];
		// 		$data['image18'] = $picture18;
		// 	}else if (isset($_FILES['fileToUpload19']) && $_FILES['fileToUpload19']['name'] != ''){
		// 		/*******Upload image 19**********/    
			    
		// 	    $pictureupload19 = $this->uploadImage('fileToUpload19');
		// 		$picture19 = $pictureupload19['upload_data']['file_name'];
		// 		$data['image19'] = $picture19;
		// 	}else if (isset($_FILES['fileToUpload20']) && $_FILES['fileToUpload20']['name'] != ''){
		// 		/*******Upload image 20**********/    
			    
		// 	    $pictureupload20 = $this->uploadImage('fileToUpload20');
		// 		$picture20 = $pictureupload20['upload_data']['file_name'];
		// 		$data['image20'] = $picture20;
		// 	}


		// 	$KitchenGallery = $this->api_model->KitchenGallery($data);

		// 	//print_r($UpdateuserProfile);die;
		// 	if(!empty($KitchenGallery) ){
		// 	//	echo "1";die;
		// 	$responseArray = array( 'status'=>'1', 'id'=>$KitchenGallery['id'],'message'=>ResponseMessages::getStatusCodeMessage(200));//OK
		// 	}else{
		// 		//echo "3";die;
		// 	$responseArray = array('status'=>'0' ,'id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
		// 	}
				
		// 	$response = $this->generate_response($responseArray);
		// 	$this->response($response);
  //   }


    function KitchenGallery_post(){
    $picture1=$picture2=$picture3=$picture4=$picture5=$picture6=$picture7=$picture8=$picture9=$picture10='';
    $picture11=$picture12=$picture13=$picture14=$picture15=$picture16=$picture17=$picture18 =$picture19=$picture20='';
    		$data['id']=($this->post('id')) ? $this->post('id') : '';
    		$data['user_id'] = ($this->post('chef_id')) ? $this->post('chef_id') : '0';
			
		/*****Upload profile pic*******/
		
			if (isset($_FILES['fileToUpload1']) && $_FILES['fileToUpload1']['name'] != ''){
				/*******Upload image 1**********/
			    
			    $pictureupload1 = $this->uploadImage('fileToUpload1');
				$picture1 = $pictureupload1['upload_data']['file_name'];
				$data['image1'] = $picture1;
			}else if (isset($_FILES['fileToUpload2']) && $_FILES['fileToUpload2']['name'] != ''){
				/*******Upload image 2**********/    
			    
			    $pictureupload2 = $this->uploadImage('fileToUpload2');
				$picture2 = $pictureupload2['upload_data']['file_name'];
				$data['image2'] = $picture2;
			}else if (isset($_FILES['fileToUpload3']) && $_FILES['fileToUpload3']['name'] != ''){
				/*******Upload image 3**********/    
			    
			    $pictureupload3 = $this->uploadImage('fileToUpload3');
				$picture3 = $pictureupload3['upload_data']['file_name'];
				$data['image3'] = $picture3;
			}else if (isset($_FILES['fileToUpload4']) && $_FILES['fileToUpload4']['name'] != ''){
				/*******Upload image 4**********/    
			    
			    $pictureupload4 = $this->uploadImage('fileToUpload4');
				$picture4 = $pictureupload4['upload_data']['file_name'];
				$data['image4'] = $picture4;
			}else if (isset($_FILES['fileToUpload5']) && $_FILES['fileToUpload5']['name'] != ''){
				/*******Upload image 5**********/    
			    
			    $pictureupload5 = $this->uploadImage('fileToUpload5');
				$picture5 = $pictureupload5['upload_data']['file_name'];
				$data['image5'] = $picture5;
			}else if (isset($_FILES['fileToUpload6']) && $_FILES['fileToUpload6']['name'] != ''){
				/*******Upload image 6**********/    
			    
			    $pictureupload6 = $this->uploadImage('fileToUpload6');
				$picture6 = $pictureupload6['upload_data']['file_name'];
				$data['image6'] = $picture6;
			}else if (isset($_FILES['fileToUpload7']) && $_FILES['fileToUpload7']['name'] != ''){
				/*******Upload image 7**********/    
			    
			    $pictureupload7 = $this->uploadImage('fileToUpload7');
				$picture7 = $pictureupload7['upload_data']['file_name'];
				$data['image7'] = $picture7;
			}else if (isset($_FILES['fileToUpload8']) && $_FILES['fileToUpload8']['name'] != ''){
				/*******Upload image 8**********/    
			    
			    $pictureupload8 = $this->uploadImage('fileToUpload8');
				$picture8 = $pictureupload8['upload_data']['file_name'];
				$data['image8'] = $picture8;
			}else if (isset($_FILES['fileToUpload9']) && $_FILES['fileToUpload9']['name'] != ''){
				/*******Upload image 9**********/    
			    
			    $pictureupload9 = $this->uploadImage('fileToUpload9');
				$picture9 = $pictureupload9['upload_data']['file_name'];
				$data['image9'] = $picture9;
			}else if (isset($_FILES['fileToUpload10']) && $_FILES['fileToUpload10']['name'] != ''){
				/*******Upload image 10**********/    
			    
			    $pictureupload10 = $this->uploadImage('fileToUpload10');
				$picture10 = $pictureupload10['upload_data']['file_name'];
				$data['image10'] = $picture10;
			}else if (isset($_FILES['fileToUpload11']) && $_FILES['fileToUpload11']['name'] != ''){
				/*******Upload image 11**********/
			    
			    $pictureupload11 = $this->uploadImage('fileToUpload11');
				$picture11 = $pictureupload11['upload_data']['file_name'];
				$data['image11'] = $picture11;
			}else if (isset($_FILES['fileToUpload12']) && $_FILES['fileToUpload12']['name'] != ''){
				/*******Upload image 12**********/    
			   
			    $pictureupload12 = $this->uploadImage('fileToUpload12');
				$picture12 = $pictureupload12['upload_data']['file_name'];
				$data['image12'] = $picture12;
			}else if (isset($_FILES['fileToUpload13']) && $_FILES['fileToUpload13']['name'] != ''){
				/*******Upload image 13**********/    
			    
			    $pictureupload13 = $this->uploadImage('fileToUpload13');
				$picture13 = $pictureupload13['upload_data']['file_name'];
				$data['image13'] = $picture13;
			}else if (isset($_FILES['fileToUpload14']) && $_FILES['fileToUpload14']['name'] != ''){
				/*******Upload image 14**********/    
			    
			    $pictureupload14 = $this->uploadImage('fileToUpload14');
				$picture14 = $pictureupload14['upload_data']['file_name'];
				$data['image14'] = $picture14;
			}else if (isset($_FILES['fileToUpload15']) && $_FILES['fileToUpload15']['name'] != ''){
				/*******Upload image 15**********/    
			    
			    $pictureupload15 = $this->uploadImage('fileToUpload15');
				$picture15 = $pictureupload15['upload_data']['file_name'];
				$data['image15'] = $picture15;
			}else if (isset($_FILES['fileToUpload16']) && $_FILES['fileToUpload16']['name'] != ''){
				/*******Upload image 16**********/    
			    
			    $pictureupload16 = $this->uploadImage('fileToUpload16');
				$picture16 = $pictureupload16['upload_data']['file_name'];
				$data['image16'] = $picture16;
			}else if (isset($_FILES['fileToUpload17']) && $_FILES['fileToUpload17']['name'] != ''){
				/*******Upload image 17**********/    
			    
			    $pictureupload17 = $this->uploadImage('fileToUpload17');
				$picture17 = $pictureupload17['upload_data']['file_name'];
				$data['image17'] = $picture17;
			}else if (isset($_FILES['fileToUpload18']) && $_FILES['fileToUpload18']['name'] != ''){
				/*******Upload image 18**********/    
			    
			    $pictureupload18 = $this->uploadImage('fileToUpload18');
				$picture18 = $pictureupload18['upload_data']['file_name'];
				$data['image18'] = $picture18;
			}else if (isset($_FILES['fileToUpload19']) && $_FILES['fileToUpload19']['name'] != ''){
				/*******Upload image 19**********/    
			    
			    $pictureupload19 = $this->uploadImage('fileToUpload19');
				$picture19 = $pictureupload19['upload_data']['file_name'];
				$data['image19'] = $picture19;
			}else if (isset($_FILES['fileToUpload20']) && $_FILES['fileToUpload20']['name'] != ''){
				/*******Upload image 20**********/    
			    
			    $pictureupload20 = $this->uploadImage('fileToUpload20');
				$picture20 = $pictureupload20['upload_data']['file_name'];
				$data['image20'] = $picture20;
			}


			$KitchenGallery = $this->api_model->KitchenGallery($data);

			//print_r($UpdateuserProfile);die;
			if(!empty($KitchenGallery) ){
			//	echo "1";die;
			$responseArray = array( 'status'=>'1', 'id'=>$KitchenGallery['id'],'message'=>ResponseMessages::getStatusCodeMessage(200));//OK
			}else{
				//echo "3";die;
			$responseArray = array('status'=>'0' ,'id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
			}
				
			$response = $this->generate_response($responseArray);
			$this->response($response);
    }



    /***********Get kitchen gallery*********/
 //    function getKitchengallery_get(){
	// 	$chefid = ($this->get('chef_id')) ? $this->get('chef_id') : '';
 //      //	echo $menuid;die;
 //    	$getKitchengallery= $this->api_model->getKitchengallery($chefid);
		
		
	// 	if(!empty($getKitchengallery)){
	// $id = $getKitchengallery[0]->id;
	// 	$chefid = $getKitchengallery[0]->chef_id;	
 //    unset($getKitchengallery[0]->id);
 //    unset($getKitchengallery[0]->chef_id);
 //    unset($getKitchengallery[0]->created_at);
 //    unset($getKitchengallery[0]->updated_at);
	// $array = array_values($getKitchengallery); //reindexing

	// 		$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'id'=>$id,
	// 			'chef_id'=>$chefid,'getKitchengallery'=>$array);
	// 		$response = $this->generate_response($responseArray);
	// 		$this->response($response);
	// 	}else{
	// 		// $array = array();
	// 		// $array['image1'] = '';
	// 		$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507)); //no records found
	// 		$response = $this->generate_response($responseArray);
	// 		$this->response($response);
	// 	}
	// }

	  function getKitchengallery_get(){
		$chefid = ($this->get('chef_id')) ? $this->get('chef_id') : '';
      //	echo $menuid;die;
    	$getKitchengallery= $this->api_model->getKitchengallery($chefid);
		
		
		if(!empty($getKitchengallery)){
	$id = $getKitchengallery[0]->id;
	$chefid = $getKitchengallery[0]->user_id;	
    unset($getKitchengallery[0]->id);
    unset($getKitchengallery[0]->user_id);
    unset($getKitchengallery[0]->created_at);
    unset($getKitchengallery[0]->updated_at);
    unset($getKitchengallery[0]->language);
    unset($getKitchengallery[0]->chef_fname);
    unset($getKitchengallery[0]->chef_lname);
    unset($getKitchengallery[0]->language_speak);
    unset($getKitchengallery[0]->service_type);
    unset($getKitchengallery[0]->phone_number);
    unset($getKitchengallery[0]->oftencook);
    unset($getKitchengallery[0]->chef_country);
    unset($getKitchengallery[0]->chef_city);
    unset($getKitchengallery[0]->from_guest);
    unset($getKitchengallery[0]->upto_guest);
    unset($getKitchengallery[0]->kitchen_title);
    unset($getKitchengallery[0]->kitchen_descrition);
    unset($getKitchengallery[0]->profile_pic);
    unset($getKitchengallery[0]->status);
    unset($getKitchengallery[0]->chef_latitude);
    unset($getKitchengallery[0]->chef_longitude);
    unset($getKitchengallery[0]->favorite);
    unset($getKitchengallery[0]->brunch);
    unset($getKitchengallery[0]->lunch);
    unset($getKitchengallery[0]->dinner);
    unset($getKitchengallery[0]->currency);
    unset($getKitchengallery[0]->cusines);	
	unset($getKitchengallery[0]->cuisine);	
	$array = array_values($getKitchengallery); //reindexing

			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'id'=>$id,
				'chef_id'=>$chefid,'getKitchengallery'=>$array);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			// $array = array();
			// $array['image1'] = '';
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507)); //no records found
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
	}

	/********Delete Kitchen Gallery********/
	function DeleteKitchenGallery_post(){
		
    		$data['id']=($this->post('id')) ? $this->post('id') : '';
    		// $data['chef_id'] = ($this->post('chef_id')) ? $this->post('chef_id') : '0';
    		$data['user_id'] = ($this->post('chef_id')) ? $this->post('chef_id') : '0';
    		$imagenumber = ($this->post('imagenumber')) ? $this->post('imagenumber') : '0';
			
		/*****Upload profile pic*******/
		
			if (isset($imagenumber) &&  $imagenumber == '1' && $imagenumber!='0'){
				/*******Upload image 1**********/
				$data['image1'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '2' && $imagenumber!='0'){
				/*******Upload image 2**********/    
			 
				$data['image2'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '3' && $imagenumber!='0'){
				/*******Upload image 3**********/    
			   
				$data['image3'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '4' && $imagenumber!='0'){
				/*******Upload image 4**********/    
			  
				$data['image4'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '5' && $imagenumber!='0'){
				/*******Upload image 5**********/    
		
				$data['image5'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '6' && $imagenumber!='0'){
				/*******Upload image 6**********/    
			   
				$data['image6'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '7' && $imagenumber!='0'){
				/*******Upload image 7**********/    
			    
				$data['image7'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '8' && $imagenumber!='0'){
				/*******Upload image 8**********/    
			    
				$data['image8'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '9' && $imagenumber!='0'){
				/*******Upload image 9**********/    
			  
				$data['image9'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '10' && $imagenumber!='0'){
				/*******Upload image 10**********/    
			   
				$data['image10'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '11' && $imagenumber!='0'){
				/*******Upload image 11**********/
			    
				$data['image11'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '12' && $imagenumber!='0'){
				/*******Upload image 12**********/    
			   
				$data['image12'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '13' && $imagenumber!='0'){
				/*******Upload image 13**********/    
			    
				$data['image13'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '14' && $imagenumber!='0'){
				/*******Upload image 14**********/    
			    
				$data['image14'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '15' && $imagenumber!='0'){
				/*******Upload image 15**********/    
			    
				$data['image15'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '16' && $imagenumber!='0'){
				/*******Upload image 16**********/    
			   
				$data['image16'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '17' && $imagenumber!='0'){
				/*******Upload image 17**********/    
			    
				$data['image17'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '18' && $imagenumber!='0'){
				/*******Upload image 18**********/    
			    
				$data['image18'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '19' && $imagenumber!='0'){
				/*******Upload image 19**********/    
			    
				$data['image19'] = '';
			}else if (isset($imagenumber) &&  $imagenumber == '20' && $imagenumber!='0'){
				/*******Upload image 20**********/    
			    
				$data['image20'] = '';
			}


			$DeleteKitchenGallery = $this->api_model->DeleteKitchenGallery($data);

			//print_r($UpdateuserProfile);die;
			if(!empty($DeleteKitchenGallery) ){
			//	echo "1";die;
			$responseArray = array( 'status'=>'1','id'=>$DeleteKitchenGallery['id'],'message'=>ResponseMessages::getStatusCodeMessage(200));//OK
			}else{
				//echo "3";die;
			$responseArray = array('status'=>'0' ,'message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
			}
				
			$response = $this->generate_response($responseArray);
			$this->response($response);
	}


	/*********store lat long of users**********/


	/*******Add Permission********/
    function userLatLong_post(){
    		$user_id = ($this->post('user_id')) ? $this->post('user_id') : '0';
			$data['latitude'] = ($this->post('latitude')) ? $this->post('latitude') : '0';
			$data['longitude'] = ($this->post('longitude')) ? $this->post('longitude') : '0';
			$data['updated_at'] = date('Y-m-d H:i:s');

			$userLatLong = $this->api_model->userLatLong($data,$user_id);
			
			if(!empty($userLatLong)){
			$responseArray = array('status'=>'1','id'=>$userLatLong['id'],'message'=>ResponseMessages::getStatusCodeMessage(200));
			}else{
			$responseArray = array('status'=>'0','id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
			}
				
			
		
		$response = $this->generate_response($responseArray);
			$this->response($response);
    }

    /********Like or dislike chef api*********/
    function favouriteChef_post(){
    		//$data['id'] = ($this->post('id')) ? $this->post('id') : '0';
    		$data['user_id'] = ($this->post('user_id')) ? $this->post('user_id') : '0';
    		$data['chef_id'] = ($this->post('chef_id')) ? $this->post('chef_id') : '0';			
			$data['favourite'] = ($this->post('favourite')) ? $this->post('favourite') : '0'; //0 -dislike and 1-like
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			if($data['favourite'] == '1'){  // like the chef
			//insert data
				$favourite = $this->api_model->favouriteChef($data);
			}else{ //delete records
				$favourite = $this->api_model->deletefavouriteChef($data);

			}
			
			
			if(!empty($favourite)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200));
			}else{
			$responseArray = array('status'=>'0','id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
			}
				
			
		
		$response = $this->generate_response($responseArray);
			$this->response($response);

    }

    /*******Get chef like or dislike**********/
    function wishlistChef_get(){
    	$userid = ($this->get('user_id')) ? $this->get('user_id') : '';
    	 $chefid = ($this->get('chef_id')) ? $this->get('chef_id') : '';
      //	echo $menuid;die;
    	 $wishlistChef= $this->api_model->wishlistChef($userid,$chefid);
    	//$wishlistChef= $this->api_model->wishlistChef($userid);
		
		
		if(!empty($wishlistChef)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(200)); //no records found
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}

    }

 

    function getCuisine_get(){
    	$language = ($this->get('language')) ? $this->get('language') : '';
    	
    	if($language !='' && $language == 'english'){
    		// $cuisine = array('cuisine_name'=>array('Hong Kong Style','Chinese','Cantonese','Taiwanese','Japanese',
    		// 'Korean','Thai','Other Asians','Italian','French','Western','Middle East/Mediterranean',
    		// 'Latin American','Multinational'));
    	$cuisine = array(array('cuisine_id'=>'1','cuisine_name'=>'Hong Kong Style',
    			'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'Hong Kong Style'))),
    				array('cuisine_id'=>'2','cuisine_name'=>'Chinese',
    				'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'All Chinese'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'Guangdong'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'Chiu Chow'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'Hakka'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'Sichuan'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'Shanghai'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'Yunnan'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'Hunan'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'Taiwan'),
    				array('subcuisine_id'=>'10','subcuisine_name'=>'Shunde'),
    				array('subcuisine_id'=>'11','subcuisine_name'=>'Beijing'),
    				array('subcuisine_id'=>'12','subcuisine_name'=>'Jingchuanhu'),
    				array('subcuisine_id'=>'13','subcuisine_name'=>'Guangxi'),
    				array('subcuisine_id'=>'14','subcuisine_name'=>'Northeastern'),
    				array('subcuisine_id'=>'15','subcuisine_name'=>'Shandong'),
    				array('subcuisine_id'=>'16','subcuisine_name'=>'Xinjiang'),
    				array('subcuisine_id'=>'17','subcuisine_name'=>'Village Food'),
    				array('subcuisine_id'=>'18','subcuisine_name'=>'Fujian'),
    				array('subcuisine_id'=>'19','subcuisine_name'=>'Jiang-Zhe'),
    				array('subcuisine_id'=>'20','subcuisine_name'=>'Guizhou'),
    				array('subcuisine_id'=>'21','subcuisine_name'=>'Shanxi(Shan)'),
    				array('subcuisine_id'=>'22','subcuisine_name'=>'Mongolia'),
    				array('subcuisine_id'=>'23','subcuisine_name'=>'Shanxi(Jin)'),
    				array('subcuisine_id'=>'24','subcuisine_name'=>'Hubei'),
    				array('subcuisine_id'=>'25','subcuisine_name'=>'Huaiyang'))),
    			array('cuisine_id'=>'3','cuisine_name'=>'Cantonese',
    				'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'All Cantonese'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'Guangdong'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'Chiu Chow'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'Hakka'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'Shunde'))),
    			array('cuisine_id'=>'4','cuisine_name'=>'Taiwanese',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'Taiwanese'))),
    			array('cuisine_id'=>'5','cuisine_name'=>'Japanese',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'Japanese'))),
    			array('cuisine_id'=>'6','cuisine_name'=>'Korean',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'Korean'))),
    			array('cuisine_id'=>'7','cuisine_name'=>'Thai',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'Thai'))),
    			array('cuisine_id'=>'8','cuisine_name'=>'Other Asians',
    				'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'All Asian'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'Vietnamese'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'Indonesian'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'Singaporean'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'Malaysian'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'Philippines'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'Indian'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'Nepalese'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'Sri Lanka'))),
    			array('cuisine_id'=>'9','cuisine_name'=>'Italian',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'Italian'))),
    			array('cuisine_id'=>'10','cuisine_name'=>'French',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'French'))),
    			array('cuisine_id'=>'11','cuisine_name'=>'Western',
    				'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'All Western'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'Italian'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'French'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'American'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'British'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'Spanish'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'German'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'Belgian'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'Australian'),
    				array('subcuisine_id'=>'10','subcuisine_name'=>'Portuguese'),
    				array('subcuisine_id'=>'11','subcuisine_name'=>'Swiss'),
    				array('subcuisine_id'=>'12','subcuisine_name'=>'Irish'),
    				array('subcuisine_id'=>'13','subcuisine_name'=>'Russian'),
    				array('subcuisine_id'=>'14','subcuisine_name'=>'Dutch'),
    				array('subcuisine_id'=>'15','subcuisine_name'=>'Austrian'),
    				array('subcuisine_id'=>'16','subcuisine_name'=>'Western'))),
    			array('cuisine_id'=>'12','cuisine_name'=>'Middle East/Mediterranean',
    				'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'All Middle Eastern / Mediterranean'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'Middle Eastern'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'Mediterranean'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'Turkish'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'Lebanon'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'Moroccan'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'Egyptian'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'Afrian'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'Jewish'),
    				array('subcuisine_id'=>'10','subcuisine_name'=>'Greek'))),
    			array('cuisine_id'=>'13','cuisine_name'=>'Latin American',
    				'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'All Latin American'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'Mexican'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'Cuba'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'Aargentinian'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'Peruvian'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'Brazillian'))),
    			array('cuisine_id'=>'14','cuisine_name'=>'Multinational',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'Multinational')))
    			);
    	}elseif($language !='' && $language == 'simplified-chinese'){
    	
    	$cuisine = array(array('cuisine_id'=>'1','cuisine_name'=>'港式菜',
    			'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'港式菜'))), //Hong Kong Style
    		array('cuisine_id'=>'2','cuisine_name'=>'中国菜',
    				'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有中国菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'粤菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'潮州菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'客家菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'四川菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'上海菜'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'云南菜'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'湖南菜'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'台湾菜'),
    				array('subcuisine_id'=>'10','subcuisine_name'=>'顺德菜'),
    				array('subcuisine_id'=>'11','subcuisine_name'=>'北京菜'),
    				array('subcuisine_id'=>'12','subcuisine_name'=>'京川菜'),
    				array('subcuisine_id'=>'13','subcuisine_name'=>'广西菜'),
    				array('subcuisine_id'=>'14','subcuisine_name'=>'东北菜'),
    				array('subcuisine_id'=>'15','subcuisine_name'=>'山东菜'),
    				array('subcuisine_id'=>'16','subcuisine_name'=>'新疆菜'),
    				array('subcuisine_id'=>'17','subcuisine_name'=>'乡村菜'),
    				array('subcuisine_id'=>'18','subcuisine_name'=>'福建菜'),
    				array('subcuisine_id'=>'19','subcuisine_name'=>'哲江菜'),
    				array('subcuisine_id'=>'20','subcuisine_name'=>'贵州菜'),
    				array('subcuisine_id'=>'21','subcuisine_name'=>'山西菜（山）'),
    				array('subcuisine_id'=>'22','subcuisine_name'=>'蒙古菜'),
    				array('subcuisine_id'=>'23','subcuisine_name'=>'山西菜（晋）'),
    				array('subcuisine_id'=>'24','subcuisine_name'=>'湖北菜'),
    				array('subcuisine_id'=>'25','subcuisine_name'=>'淮阳菜'))), //Chinese
    		array('cuisine_id'=>'3','cuisine_name'=>'广东话',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有广东菜'),
					array('subcuisine_id'=>'2','subcuisine_name'=>'粤菜'),
					array('subcuisine_id'=>'3','subcuisine_name'=>'潮州菜'),
					array('subcuisine_id'=>'4','subcuisine_name'=>'客家菜'),
					array('subcuisine_id'=>'5','subcuisine_name'=>'顺德菜'))), //Cantonese
			array('cuisine_id'=>'4','cuisine_name'=>'广东菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'广东菜'))), //Taiwanese
			array('cuisine_id'=>'5','cuisine_name'=>'日本菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'日本菜'))), //Japanese
			array('cuisine_id'=>'6','cuisine_name'=>'韩国菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'韩国菜'))), //Korean
			array('cuisine_id'=>'7','cuisine_name'=>'泰国菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'泰国菜'))), //Thai
			array('cuisine_id'=>'8','cuisine_name'=>'其他亚洲菜',
    				'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有亚洲菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'越南菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'印度尼西亚菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'新加坡菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'马来西亚菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'菲律宾菜'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'印度菜'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'尼泊尔菜'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'斯里兰卡菜'))), //Other Asians
			array('cuisine_id'=>'9','cuisine_name'=>'意大利菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'意大利菜'))), //Italian
			array('cuisine_id'=>'10','cuisine_name'=>'法国菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'法国菜'))), //French
			array('cuisine_id'=>'11','cuisine_name'=>'西式菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有的西式菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'意大利菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'法国菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'美国菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'英国菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'西班牙菜'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'德国菜'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'比利时菜'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'澳洲菜'),
    				array('subcuisine_id'=>'10','subcuisine_name'=>'葡萄牙菜'),
    				array('subcuisine_id'=>'11','subcuisine_name'=>'瑞士菜'),
    				array('subcuisine_id'=>'12','subcuisine_name'=>'爱尔兰菜'),
    				array('subcuisine_id'=>'13','subcuisine_name'=>'俄国菜'),
    				array('subcuisine_id'=>'14','subcuisine_name'=>'荷兰菜'),
    				array('subcuisine_id'=>'15','subcuisine_name'=>'奥地利菜'),
    				array('subcuisine_id'=>'16','subcuisine_name'=>'西式菜'))), //Western
			array('cuisine_id'=>'12','cuisine_name'=>'中东/地中海菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有的中东/地中海菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'中东菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'地中海菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'土耳其菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'黎巴嫩菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'摩洛哥菜'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'埃及菜'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'非洲菜'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'犹太菜'),
    				array('subcuisine_id'=>'10','subcuisine_name'=>'希腊菜'))), //Middle East/Mediterranean
			array('cuisine_id'=>'13','cuisine_name'=>'拉丁美洲菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有的拉丁美洲菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'墨西哥菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'古巴菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'阿根廷菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'秘鲁菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'巴西菜'))), //Latin American
			array('cuisine_id'=>'14','cuisine_name'=>'多国菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'多国菜')))); //Multinational
    	}else{
    		// $cuisine = array('cuisine_name'=>array('Hong Kong Style','中文','廣東話','台灣','Japanese',
    		// 'Korean','Thai','Other Asians','Italian','French','Western','Middle East/Mediterranean',
    		// 'Latin American','Multinational'));
    $cuisine = array(array('cuisine_id'=>'1','cuisine_name'=>'港式菜',
    			'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'港式菜'))), //Hong Kong Style
    	array('cuisine_id'=>'2','cuisine_name'=>'中國菜',
    			'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有中國菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'粵菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'潮州菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'客家菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'四川菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'上海菜'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'雲南菜'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'湖南菜'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'台灣菜'),
    				array('subcuisine_id'=>'10','subcuisine_name'=>'順德菜'),
    				array('subcuisine_id'=>'11','subcuisine_name'=>'北京菜'),
    				array('subcuisine_id'=>'12','subcuisine_name'=>'京川菜'),
    				array('subcuisine_id'=>'13','subcuisine_name'=>'廣西菜'),
    				array('subcuisine_id'=>'14','subcuisine_name'=>'東北菜'),
    				array('subcuisine_id'=>'15','subcuisine_name'=>'山東菜'),
    				array('subcuisine_id'=>'16','subcuisine_name'=>'新疆菜'),
    				array('subcuisine_id'=>'17','subcuisine_name'=>'鄉村菜'),
    				array('subcuisine_id'=>'18','subcuisine_name'=>'福建菜'),
    				array('subcuisine_id'=>'19','subcuisine_name'=>'哲江菜'),
    				array('subcuisine_id'=>'20','subcuisine_name'=>'貴州菜'),
    				array('subcuisine_id'=>'21','subcuisine_name'=>'山西菜（山）'),
    				array('subcuisine_id'=>'22','subcuisine_name'=>'蒙古菜'),
    				array('subcuisine_id'=>'23','subcuisine_name'=>'山西菜（晉）'),
    				array('subcuisine_id'=>'24','subcuisine_name'=>'湖北菜'),
    				array('subcuisine_id'=>'25','subcuisine_name'=>'淮陽菜'))), //Chinese
    	array('cuisine_id'=>'3','cuisine_name'=>'廣東菜',
    				'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有廣東菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'粵菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'潮州菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'客家菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'順德菜'))), //Cantonese
		array('cuisine_id'=>'4','cuisine_name'=>'台灣菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'台灣菜'))), //Taiwanese
		array('cuisine_id'=>'5','cuisine_name'=>'日本菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'日本菜'))), //Japanese
		array('cuisine_id'=>'6','cuisine_name'=>'韓國菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'韓國菜'))), //Korean
		array('cuisine_id'=>'7','cuisine_name'=>'泰國菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'泰國菜'))), //Thai
		array('cuisine_id'=>'8','cuisine_name'=>'其他亞洲菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有亞洲菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'越南菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'印度尼西亞菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'新加坡菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'馬來西亞菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'菲律賓菜'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'印度菜'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'尼泊爾菜'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'斯里蘭卡菜'))), //Other Asians
		array('cuisine_id'=>'9','cuisine_name'=>'意大利菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'意大利菜'))), //Italian
		array('cuisine_id'=>'10','cuisine_name'=>'法國菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'法國菜'))), //French
		array('cuisine_id'=>'11','cuisine_name'=>'西式菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有的西式菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'意大利菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'法國菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'美國菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'英國菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'西班牙菜'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'德國菜'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'比利時菜'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'澳洲菜'),
    				array('subcuisine_id'=>'10','subcuisine_name'=>'葡萄牙菜'),
    				array('subcuisine_id'=>'11','subcuisine_name'=>'瑞士菜'),
    				array('subcuisine_id'=>'12','subcuisine_name'=>'愛爾蘭菜'),
    				array('subcuisine_id'=>'13','subcuisine_name'=>'俄國菜'),
    				array('subcuisine_id'=>'14','subcuisine_name'=>'荷蘭菜'),
    				array('subcuisine_id'=>'15','subcuisine_name'=>'奧地利菜'),
    				array('subcuisine_id'=>'16','subcuisine_name'=>'西式菜'))), //Western
		array('cuisine_id'=>'12','cuisine_name'=>'中東/地中海菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有的中東/地中海菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'中東菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'地中海菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'土耳其菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'黎巴嫩菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'摩洛哥菜'),
    				array('subcuisine_id'=>'7','subcuisine_name'=>'埃及菜'),
    				array('subcuisine_id'=>'8','subcuisine_name'=>'比利時菜'),
    				array('subcuisine_id'=>'9','subcuisine_name'=>'猶太菜'),
    				array('subcuisine_id'=>'10','subcuisine_name'=>'希臘菜'))), //Middle East/Mediterranean
		array('cuisine_id'=>'13','cuisine_name'=>'拉丁美洲菜',
					'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'所有的拉丁美洲菜'),
    				array('subcuisine_id'=>'2','subcuisine_name'=>'墨西哥菜'),
    				array('subcuisine_id'=>'3','subcuisine_name'=>'古巴菜'),
    				array('subcuisine_id'=>'4','subcuisine_name'=>'阿根廷菜'),
    				array('subcuisine_id'=>'5','subcuisine_name'=>'秘魯菜'),
    				array('subcuisine_id'=>'6','subcuisine_name'=>'巴西菜'))), //Latin American
		array('cuisine_id'=>'14','cuisine_name'=>'多國菜',
		'subCuisne'=>array(array('subcuisine_id'=>'1','subcuisine_name'=>'多國菜')))
		); //Multinational
    	}
    	
    	if(!empty($cuisine)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'cuisine'=>$cuisine);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(200)); //no records found
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
    }

 

    /*********Api for get menus and submenus together**********/
    function getMenusSubmenus_get(){
    	$userid = ($this->get('user_id')) ? $this->get('user_id') : '';
    	$getMenusSubmenus= $this->api_model->getMenusSubmenus($userid);
    	//echo '<pre>';
		foreach ($getMenusSubmenus as $key => $value) {
			/*****get dish*****/
		$menuids[] =  $value->id;
		$menuid=  $value->id;
		$MenusSubmenus[]= $this->api_model->MenusSubmenus($userid,$menuid);
			// 
		}
		
// print_r(array_filter($MenusSubmenus));die;
		//$MenusSubmenus = array_filter($MenusSubmenus);
foreach ($MenusSubmenus as $key => $value) {
	
	if(!empty($value)){
	$MenusSubmenus1[] = $value;
	}
	//print_r($MenusSubmenus1);
}

		if(!empty($MenusSubmenus1)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'MenusSubmenus'=>$MenusSubmenus1);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}

    }

    /*******Get menu list for chef profile*********/
     function ChefmenuList_get(){
    	
    	$chefid = ($this->get('chef_id')) ? $this->get('chef_id') : '';
      //	echo $menuid;die;
    	$ChefmenuList= $this->api_model->ChefmenuList($chefid);
		
		
		if(!empty($ChefmenuList)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'ChefmenuList'=>$ChefmenuList);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507)); //no records found
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}

    }


    /***********Onsite cooking option*************/

    function onsiteCooking_get(){
    	$language = ($this->get('language')) ? $this->get('language') : '';
    	if(!empty($language)  && $language == 'english'){
    		$onsiteCookingarr = array(array('onsite_id'=>'1','cooking_tool'=>'Potato masher'),
    				array('onsite_id'=>'2','cooking_tool'=>'Measuring cups/jug and spoons'),
    				array('onsite_id'=>'3','cooking_tool'=>'Chef knife'),
    				array('onsite_id'=>'4','cooking_tool'=>'Paring knife'),
    				array('onsite_id'=>'5','cooking_tool'=>'Potato ricer'),
    				array('onsite_id'=>'6','cooking_tool'=>'Oven mitt/glove'),
    				array('onsite_id'=>'7','cooking_tool'=>'Serrated or bread knife'),
    				array('onsite_id'=>'8','cooking_tool'=>'Vegetable peelers'),
    				array('onsite_id'=>'9','cooking_tool'=>'Sharping steel'),
    				array('onsite_id'=>'10','cooking_tool'=>'Garlic press'),
    				array('onsite_id'=>'11','cooking_tool'=>'BBQ tools'),
    				array('onsite_id'=>'12','cooking_tool'=>'Electric sharpener'),
    				array('onsite_id'=>'13','cooking_tool'=>'Egg separater'),
    				array('onsite_id'=>'14','cooking_tool'=>'Choppers'),
    				array('onsite_id'=>'15','cooking_tool'=>'Cutting board'),
    				array('onsite_id'=>'16','cooking_tool'=>'Fish scaler'),
    				array('onsite_id'=>'17','cooking_tool'=>'Lemon press/Lemon squeezer'),
    				array('onsite_id'=>'18','cooking_tool'=>'Ice cream scoop'),
    				array('onsite_id'=>'19','cooking_tool'=>'Pepper mill,salt & pepper shakers'),
    				array('onsite_id'=>'20','cooking_tool'=>'Tongs'),
    				array('onsite_id'=>'21','cooking_tool'=>'Rolling pin'),
    				array('onsite_id'=>'22','cooking_tool'=>'Can opener'),
    				array('onsite_id'=>'23','cooking_tool'=>'Meat grinder'),
    				array('onsite_id'=>'24','cooking_tool'=>'Salad spinner'),
    				array('onsite_id'=>'26','cooking_tool'=>'Juicer'),
    				array('onsite_id'=>'27','cooking_tool'=>'Blender'),
    				array('onsite_id'=>'28','cooking_tool'=>'Rice Cooker'),
    				array('onsite_id'=>'29','cooking_tool'=>'Toaster'),
    				array('onsite_id'=>'30','cooking_tool'=>'Water purifier'),
    				array('onsite_id'=>'31','cooking_tool'=>'Slow cooker'),
    				array('onsite_id'=>'32','cooking_tool'=>'Sandwich maker'),
    				array('onsite_id'=>'33','cooking_tool'=>'Food processor'),
    				array('onsite_id'=>'34','cooking_tool'=>'Microwave oven'),
    				array('onsite_id'=>'35','cooking_tool'=>'Bread Maker'),
    				array('onsite_id'=>'36','cooking_tool'=>'Cast Iron Skillet'),
    				array('onsite_id'=>'37','cooking_tool'=>'Dutch oven'),
    				array('onsite_id'=>'38','cooking_tool'=>'Non Stick Skillet'),
    				array('onsite_id'=>'39','cooking_tool'=>'Stock Pot'),
    				array('onsite_id'=>'40','cooking_tool'=>'Grill Pan'),
    				array('onsite_id'=>'41','cooking_tool'=>'Reemed baking sheets'),
    				array('onsite_id'=>'42','cooking_tool'=>'Saucepans'),
    				array('onsite_id'=>'43','cooking_tool'=>'Wok'),
    				array('onsite_id'=>'44','cooking_tool'=>'Family Griddle'),
    				array('onsite_id'=>'45','cooking_tool'=>'Roasting Pans'));

    	}elseif (!empty($language)  && $language == 'simplified-chinese') {
    		$onsiteCookingarr = array(array('onsite_id'=>'1','cooking_tool'=>'土豆捣碎器'),
    				array('onsite_id'=>'2','cooking_tool'=>'量杯/水壶和勺子'),
    				array('onsite_id'=>'3','cooking_tool'=>'Chef knife'),
    				array('onsite_id'=>'4','cooking_tool'=>'Paring knife'),
    				array('onsite_id'=>'5','cooking_tool'=>'Potato ricer'),
    				array('onsite_id'=>'6','cooking_tool'=>'Oven mitt/glove'),
    				array('onsite_id'=>'7','cooking_tool'=>'Serrated or bread knife'),
    				array('onsite_id'=>'8','cooking_tool'=>'Vegetable peelers'),
    				array('onsite_id'=>'9','cooking_tool'=>'Sharping steel'),
    				array('onsite_id'=>'10','cooking_tool'=>'Garlic press'),
    				array('onsite_id'=>'11','cooking_tool'=>'BBQ tools'),
    				array('onsite_id'=>'12','cooking_tool'=>'Electric sharpener'),
    				array('onsite_id'=>'13','cooking_tool'=>'Egg separater'),
    				array('onsite_id'=>'14','cooking_tool'=>'Choppers'),
    				array('onsite_id'=>'15','cooking_tool'=>'Cutting board'),
    				array('onsite_id'=>'16','cooking_tool'=>'Fish scaler'),
    				array('onsite_id'=>'17','cooking_tool'=>'Lemon press/Lemon squeezer'),
    				array('onsite_id'=>'18','cooking_tool'=>'Ice cream scoop'),
    				array('onsite_id'=>'19','cooking_tool'=>'Pepper mill,salt & pepper shakers'),
    				array('onsite_id'=>'20','cooking_tool'=>'Tongs'),
    				array('onsite_id'=>'21','cooking_tool'=>'Rolling pin'),
    				array('onsite_id'=>'22','cooking_tool'=>'Can opener'),
    				array('onsite_id'=>'23','cooking_tool'=>'Meat grinder'),
    				array('onsite_id'=>'24','cooking_tool'=>'Salad spinner'),
    				array('onsite_id'=>'26','cooking_tool'=>'Juicer'),
    				array('onsite_id'=>'27','cooking_tool'=>'Blender'),
    				array('onsite_id'=>'28','cooking_tool'=>'Rice Cooker'),
    				array('onsite_id'=>'29','cooking_tool'=>'Toaster'),
    				array('onsite_id'=>'30','cooking_tool'=>'Water purifier'),
    				array('onsite_id'=>'31','cooking_tool'=>'Slow cooker'),
    				array('onsite_id'=>'32','cooking_tool'=>'Sandwich maker'),
    				array('onsite_id'=>'33','cooking_tool'=>'Food processor'),
    				array('onsite_id'=>'34','cooking_tool'=>'Microwave oven'),
    				array('onsite_id'=>'35','cooking_tool'=>'Bread Maker'),
    				array('onsite_id'=>'36','cooking_tool'=>'Cast Iron Skillet'),
    				array('onsite_id'=>'37','cooking_tool'=>'Dutch oven'),
    				array('onsite_id'=>'38','cooking_tool'=>'Non Stick Skillet'),
    				array('onsite_id'=>'39','cooking_tool'=>'Stock Pot'),
    				array('onsite_id'=>'40','cooking_tool'=>'Grill Pan'),
    				array('onsite_id'=>'41','cooking_tool'=>'Reemed baking sheets'),
    				array('onsite_id'=>'42','cooking_tool'=>'Saucepans'),
    				array('onsite_id'=>'43','cooking_tool'=>'Wok'),
    				array('onsite_id'=>'44','cooking_tool'=>'Family Griddle'),
    				array('onsite_id'=>'45','cooking_tool'=>'Roasting Pans'));
    	}else{
    				$onsiteCookingarr = array(array('onsite_id'=>'1','cooking_tool'=>'Potato masher'),
    				array('onsite_id'=>'2','cooking_tool'=>'量杯/水壺和勺子'),
    				array('onsite_id'=>'3','cooking_tool'=>'Chef knife'),
    				array('onsite_id'=>'4','cooking_tool'=>'Paring knife'),
    				array('onsite_id'=>'5','cooking_tool'=>'Potato ricer'),
    				array('onsite_id'=>'6','cooking_tool'=>'Oven mitt/glove'),
    				array('onsite_id'=>'7','cooking_tool'=>'Serrated or bread knife'),
    				array('onsite_id'=>'8','cooking_tool'=>'Vegetable peelers'),
    				array('onsite_id'=>'9','cooking_tool'=>'Sharping steel'),
    				array('onsite_id'=>'10','cooking_tool'=>'Garlic press'),
    				array('onsite_id'=>'11','cooking_tool'=>'BBQ tools'),
    				array('onsite_id'=>'12','cooking_tool'=>'Electric sharpener'),
    				array('onsite_id'=>'13','cooking_tool'=>'Egg separater'),
    				array('onsite_id'=>'14','cooking_tool'=>'Choppers'),
    				array('onsite_id'=>'15','cooking_tool'=>'Cutting board'),
    				array('onsite_id'=>'16','cooking_tool'=>'Fish scaler'),
    				array('onsite_id'=>'17','cooking_tool'=>'Lemon press/Lemon squeezer'),
    				array('onsite_id'=>'18','cooking_tool'=>'Ice cream scoop'),
    				array('onsite_id'=>'19','cooking_tool'=>'Pepper mill,salt & pepper shakers'),
    				array('onsite_id'=>'20','cooking_tool'=>'Tongs'),
    				array('onsite_id'=>'21','cooking_tool'=>'Rolling pin'),
    				array('onsite_id'=>'22','cooking_tool'=>'Can opener'),
    				array('onsite_id'=>'23','cooking_tool'=>'Meat grinder'),
    				array('onsite_id'=>'24','cooking_tool'=>'Salad spinner'),
    				array('onsite_id'=>'26','cooking_tool'=>'Juicer'),
    				array('onsite_id'=>'27','cooking_tool'=>'Blender'),
    				array('onsite_id'=>'28','cooking_tool'=>'Rice Cooker'),
    				array('onsite_id'=>'29','cooking_tool'=>'Toaster'),
    				array('onsite_id'=>'30','cooking_tool'=>'Water purifier'),
    				array('onsite_id'=>'31','cooking_tool'=>'Slow cooker'),
    				array('onsite_id'=>'32','cooking_tool'=>'Sandwich maker'),
    				array('onsite_id'=>'33','cooking_tool'=>'Food processor'),
    				array('onsite_id'=>'34','cooking_tool'=>'Microwave oven'),
    				array('onsite_id'=>'35','cooking_tool'=>'Bread Maker'),
    				array('onsite_id'=>'36','cooking_tool'=>'Cast Iron Skillet'),
    				array('onsite_id'=>'37','cooking_tool'=>'Dutch oven'),
    				array('onsite_id'=>'38','cooking_tool'=>'Non Stick Skillet'),
    				array('onsite_id'=>'39','cooking_tool'=>'Stock Pot'),
    				array('onsite_id'=>'40','cooking_tool'=>'Grill Pan'),
    				array('onsite_id'=>'41','cooking_tool'=>'Reemed baking sheets'),
    				array('onsite_id'=>'42','cooking_tool'=>'Saucepans'),
    				array('onsite_id'=>'43','cooking_tool'=>'Wok'),
    				array('onsite_id'=>'44','cooking_tool'=>'Family Griddle'),
    				array('onsite_id'=>'45','cooking_tool'=>'Roasting Pans'));
    	}


    	if(!empty($onsiteCookingarr)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'onsiteCookingarr'=>$onsiteCookingarr);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}

    }

    /********API for Get favroites chef******/
    function getfavroiteChef_get(){
    	$user_id = ($this->get('user_id')) ? $this->get('user_id') : '';
     // / 	echo $user_id;die;
    	$getfavroite = $this->api_model->getfavroite($user_id);
	//	echo "<pre>";
		//print_r($getfavroite);
		$chefidarr = array();
		 foreach ($getfavroite as $key => $value) {
		 	//print_r($value);
		 	$chefidarr[] = $value->chef_id;
		 }
		// / print_r(array_unique($chefidarr));

		 $chefids = implode(",", array_unique($chefidarr));
		// echo $chefids;

		 $getchefprofile = $this->api_model->getchefprofile($chefids);


		 $getfave = $this->api_model->getfave();
    	
		   if(!empty($getfave)){
		   	foreach ($getfave as $key => $value) {
		    		$chefarr[] = $value->chef_id;
		    		$chefarrq=array_count_values($chefarr);
		    	}
		    }else{
		    	$chefarrq='';
		    }
    	//print_r($chefarrq);die;
    	$getprice = $this->api_model->getprice();
    	if(!empty($getprice)){
		   	foreach ($getprice as $key => $value) {
		    		$getless[$value->id] = $value->price_per_person;
		    		
		    	}
		    }else{
		    	$getless='';
		    }
			if(!empty($getchefprofile)){
					foreach ($getchefprofile as $key => $value) {
			    	if($chefarrq !=''){
			    	// /	echo $value->id;die;
			    	if(array_key_exists($value->id, $chefarrq)){
					 	$getchefprofile[$key]->favouritecount = $chefarrq[$value->id];
				    } else{
				    	$getchefprofile[$key]->favouritecount =0;
				    }
			    	}
				   else{
				    	$getchefprofile[$key]->favouritecount =0;
				    }

				    if($getless !=''){
			    	if(array_key_exists($value->id, $getless)){
					 	$getchefprofile[$key]->price = $getless[$value->id];
				    } else{
				    	$getchefprofile[$key]->price =0;
				    }
			    	}
				   else{
				    	$getchefprofile[$key]->price =0;
				    }
			    	}

			}
	    
		
		if(!empty($getchefprofile)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'getchefprofile'=>$getchefprofile);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507)); //no records found
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}
    }
    
    /********How it works*********/
    function howitworks_get(){
    		$language = ($this->get('language')) ? $this->get('language') : '';
    		$url = base_url().'assets/howitworks/1.jpg';
    		$url1 = base_url().'assets/howitworks/2.jpg';
    		$url2 = base_url().'assets/howitworks/3.jpg';
    		$url3 = base_url().'assets/howitworks/4.jpg';
    	if(!empty($language)  && $language == 'english'){
    	$howitworks = array(array('itwork_id'=>'1','title'=>'Step 1','image'=>base_url().'assets/howitworks/1.jpg','description'=>'Choose the date and time for service'),
		array('itwork_id'=>'2','title'=>'Step 2','image'=>base_url().'assets/howitworks/2.jpg','description'=>'Select a menu and the service type'),
		array('itwork_id'=>'3','title'=>'Step 3','image'=>base_url().'assets/howitworks/3.jpg','description'=>'Chef confirms your request within 24 hours'),
		array('itwork_id'=>'4','title'=>'Step 4','image'=>base_url().'assets/howitworks/4.jpg','description'=>'Proceed with a secure payment'),
		array('itwork_id'=>'5','title'=>'Step 5','image'=>base_url().'assets/howitworks/1.jpg','description'=>'Chef provides the service and you enjoy the meal with other foodies'));

    	}elseif (!empty($language)  && $language == 'simplified-chinese') {
    	$howitworks = array(array('itwork_id'=>'1','title'=>'步骤1','image'=>base_url().'assets/howitworks/1.jpg','description'=>'选择服务的日期和时间'),
		array('itwork_id'=>'2','title'=>'第2步','image'=>base_url().'assets/howitworks/2.jpg','description'=>'选择菜单和服务类型'),
		array('itwork_id'=>'3','title'=>'步骤3','image'=>base_url().'assets/howitworks/3.jpg','description'=>'厨师24小时内确认您的要求'),
		array('itwork_id'=>'4','title'=>'步骤4','image'=>base_url().'assets/howitworks/4.jpg','description'=>'执行安全付款'),
		array('itwork_id'=>'5','title'=>'步骤5','image'=>base_url().'assets/howitworks/1.jpg','description'=>'厨师提供服务，您可以和其他美食家一起享用餐点'));
    			
    				
    	}else{
    	$howitworks = array(array('itwork_id'=>'1','title'=>'步驟1','image'=>base_url().'assets/howitworks/1.jpg','description'=>'擇服務的日期和時間'),
		array('itwork_id'=>'2','title'=>'第2步','image'=>base_url().'assets/howitworks/2.jpg','description'=>'選擇菜單和服務類型'),
		array('itwork_id'=>'3','title'=>'步驟3','image'=>base_url().'assets/howitworks/3.jpg','description'=>'廚師24小時內確認您的要求'),
		array('itwork_id'=>'4','title'=>'步驟4','image'=>base_url().'assets/howitworks/4.jpg','description'=>'執行安全付款'),
    	array('itwork_id'=>'5','title'=>'步驟5','image'=>base_url().'assets/howitworks/1.jpg','description'=>'廚師提供服務，您可以和其他美食家一起享用餐點'));
    	}


    	if(!empty($howitworks)){
			$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'howitworks'=>$howitworks);
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}else{
			$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507));
			$response = $this->generate_response($responseArray);
			$this->response($response);
		}

    }

    /********Book now*********/
    function bookNow_post(){
    	$key='';
	
		$length = 5;
		if($this->post('booking_id') !=''){
			$key = $this->post('booking_id');
		}else{
			for($i=0; $i < $length; $i++) {
		        //$key .= $pool[mt_rand(0, count($pool) - 1)];
				$key .= mt_rand(0, 10);
		    }
		}
			$data['booking_id'] = $key;
			$data['booknow_id'] = ($this->post('booknow_id')) ? $this->post('booknow_id') : '';
			$data['chef_id'] = ($this->post('chef_id')) ? $this->post('chef_id') : '';
			$data['user_id'] = ($this->post('user_id')) ? $this->post('user_id') : '';
			$data['date'] = ($this->post('date')) ? $this->post('date') : '';
			$data['time'] = ($this->post('time')) ? $this->post('time') : '';
			$data['total_seats'] = ($this->post('total_seats')) ? $this->post('total_seats') : '';
			$data['menu_id'] = ($this->post('menu_id')) ? $this->post('menu_id') : '';
			$data['menu_price'] = ($this->post('menu_price')) ? $this->post('menu_price') : '';
			$data['menu_currency'] = ($this->post('menu_currency')) ? $this->post('menu_currency') : '';
			$data['service_type'] = ($this->post('service_type')) ? $this->post('service_type') : '';
			$data['food_delivery'] = ($this->post('food_delivery')) ? $this->post('food_delivery') : '';
			$data['kitchen_tool'] = ($this->post('kitchen_tool')) ? $this->post('kitchen_tool') : ''; //comma separated
			$data['first_name'] = ($this->post('first_name')) ? $this->post('first_name') : '';
			$data['last_name'] = ($this->post('last_name')) ? $this->post('last_name') : '';
			$data['email'] = ($this->post('email')) ? $this->post('email') : '';
			$data['contact_number'] = ($this->post('contact_number')) ? $this->post('contact_number') : '';
			$data['delivery_address'] = ($this->post('delivery_address')) ? $this->post('delivery_address') : '';
			$data['user_msg'] = ($this->post('user_msg'))? $this->post('user_msg'):'';
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$data['status']=1;

			$bookNow = $this->api_model->bookNow($data);
			
			if(!empty($bookNow)){
			$responseArray = array('status'=>'1','booknow_id'=>$bookNow['booknow_id'],'booking_id'=>$bookNow['booking_id'],'message'=>ResponseMessages::getStatusCodeMessage(200));
			}else{
			$responseArray = array('status'=>'0','id'=>'','message'=>ResponseMessages::getStatusCodeMessage(118)); //something went wrong
			
			}

		
		$response = $this->generate_response($responseArray);
			$this->response($response);
    }

    /********Facebook login and register**********/
    function FBlogin_post(){
    	/*******Generate refferal code*******/
		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$res = "";
		for ($i = 0; $i < 10; $i++) {
		    $res .= $chars[mt_rand(0, strlen($chars)-1)];
		}
		/*$language = $this->post('language');*/
    		$data['fb_auth_id'] = ($this->post('id')) ? $this->post('id') : '';
			$data['first_name'] = ($this->post('first_name')) ? $this->post('first_name') : '';
			$data['last_name'] = ($this->post('last_name')) ? $this->post('last_name') : '';
			$data['email'] = ($this->post('email')) ? $this->post('email') : '';
			$data['facebook_login'] = 'yes';
			$data['status'] = '1';
			$data['is_user'] = '1';
		    $data['referal_code'] = $res;
		    $data['language'] = ($this->post('language')) ? $this->post('language') : 'english';
		// /	$data['profile_pic'] = ($this->post('picture')) ? $this->post('picture') : '';
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');

			/******Chek data exist****/
			$authid = $data['fb_auth_id'];
			$checktoken = $this->api_model->checktoken($authid);
			// echo '<pre>';
			// print_r($checktoken);
			// echo $checktoken[0]->id; die;
			if(!empty($checktoken)){
				$this->db->where('fb_auth_id',$data['fb_auth_id']);
    			$issaved =	$this->db->update('register', $data);
    			$checktoken1 = $this->api_model->checktoken($data['fb_auth_id']);
    			if(!empty($checktoken1)){
    					$session_data=array('userId'=>$checktoken1[0]->id,
						'first_name'=>$checktoken1[0]->first_name,
		            	'last_name'=>$checktoken1[0]->last_name,
		            	'email'=>$checktoken1[0]->email,
		            	'referal_code'=>$checktoken1[0]->referal_code,
		            	'is_chef'=>$checktoken1[0]->is_chef,
		            	'is_verify_chef'=>$checktoken1[0]->is_verify_chef,
		            	'phone_number'=>$checktoken1[0]->phone_number
		            	);
    			$this->session->set_userdata($session_data);
    		}else{
    			// $session_data = 'Not verified';
    				$session_data = '';
    		}
    		
    			if(!empty($session_data)){
					$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'userdata'=>$session_data);
					$response = $this->generate_response($responseArray);
					$this->response($response);
				}else{
					$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(511));
					$response = $this->generate_response($responseArray);
					$this->response($response);
				

    			}
			}else{
			//	echo 'else';die;

				$issaved = $this->db->insert('register',$data);
	            $sid = $this->db->insert_id();
	            $getdatafb = $this->api_model->getdatafb($sid);
	            if(!empty($getdatafb)){
	            	 $session_data=array('userId'=>$getdatafb[0]->id,
						'first_name'=>$getdatafb[0]->first_name,
		            	'last_name'=>$getdatafb[0]->last_name,
		            	'email'=>$getdatafb[0]->email,
		            	'referal_code'=>$getdatafb[0]->referal_code,
		            	'is_chef'=>$getdatafb[0]->is_chef,
		            	'is_verify_chef'=>$getdatafb[0]->is_verify_chef,
		            	'phone_number'=>$getdatafb[0]->phone_number
		            	);
	            	 $this->session->set_userdata($session_data);
	            }else{
	            	// $session_data = 'Not verified';
	            	$session_data = '';
	            }
	                    
		        
		     

    			if(!empty($session_data)){
					$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'userdata'=>$session_data);
					$response = $this->generate_response($responseArray);
					$this->response($response);
				}else{
					$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(511));
					$response = $this->generate_response($responseArray);
					$this->response($response);

			}
			

    }
}

    /******Instagram login********/
    function InstagramLogin_post(){
    	$userid = $this->post('user_id');
    	$data['access_token']= ($this->post('access_token')) ? $this->post('access_token') : '';
    	$updatetoken = $this->api_model->updateInstagramtoken($userid,$data);
    	if(!empty($updatetoken)){
					$responseArray = array('status'=>'1','message'=>ResponseMessages::getStatusCodeMessage(200),'updatetoken'=>$updatetoken);
					$response = $this->generate_response($responseArray);
					$this->response($response);
		}else{
					$responseArray = array('status'=>'0','message'=>ResponseMessages::getStatusCodeMessage(507));
					$response = $this->generate_response($responseArray);
					$this->response($response);
		}
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
