<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();

		header("Expires: Tue, 01 Jan 2013 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$this->load->library('form_validation');
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		$this->load->model('user_admin');
        $this->load->library('encrypt');
        $this->load->helper('language');
       //echo $this->session->userdata('site_lang');
        if($this->session->userdata('site_lang') !=''){
        $lan =$this->session->userdata('site_lang');
        $this->lang->load('message',$lan);
        }else{
            $this->lang->load('message','english');
        }
	}
	
	public function index()
	{
       // echo $lan;

		if ($this->session->userdata('admin_id')) {
            redirect('admin/main');
			//$this->load->view('admin/main',$langdata);
        } else {
        	redirect('admin/login');
        	//$this->load->view('admin/login',$langdata);
        }
	}

	public function main()
	{
       // echo $this->session->userdata('site_lang');
        // echo($this->session->userdata('select_lang'));echo($this->session->userdata('default'));die;
		if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           //  echo $this->session->userdata('site_lang');die;
			$data['page']='admindash';
			$this->load->view('admin/main',$data);
        }
	}

	public function login() {
    
        if ($this->session->userdata('admin_id')) {
			redirect('admin/main');
        } else {

            $user = $this->input->post('username');
            $password = $this->input->post('password');

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/login');
            } else {
				$data['username'] = $this->input->post('username');
				$data['password'] = md5($this->input->post('password'));
				$isAdminLoggedIn = $this->user_admin->isAdminLoggedIn($data['username'],$data['password']);
				if($isAdminLoggedIn && $this->session->userdata('admin_id') !=''){
					redirect('admin/main');
				}else {
                    $data['error'] = '<strong>Access Denied</strong> Invalid Username/Password';
                    $this->load->view('admin/login', $data);
                }
            }
        }
	}
	
    function switchLang($language = "") {
        
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        
        redirect($_SERVER['HTTP_REFERER']);
        
    }

    /*********Privacy Policy*******/
    function privacyPolicy(){
            if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
          //  echo $this->session->userdata('site_lang');die;
            $sitelang = $this->session->userdata('site_lang');
            $userid = $this->session->userdata('admin_id');
            //echo $sitelang;die;
            $data['getprivacypolicy'] = $this->user_admin->getpolicy($sitelang,$userid);
            $data['page']='privacypolicy';
            $this->load->view('admin/privacypolicy',$data);
        }
    }

    /*******Terms & Conditions********/
	function termsConditions(){
            if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $sitelang = $this->session->userdata('site_lang');
            $userid = $this->session->userdata('admin_id');
            //echo $sitelang;die;
            $data['gettermsconditions'] = $this->user_admin->gettermsconditions($sitelang,$userid);//   echo $this->session->userdata('site_lang');
            $data['page']='termsconditions';
            $this->load->view('admin/termsconditions',$data);
        }
    }

    /*****Contact******/
    function contact(){
    if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $language = $this->session->userdata('site_lang');
            $userid = $this->session->userdata('admin_id');
            $data['getcontact']= $this->user_admin->getcontact($language,$userid);
            $data['page']='contact';
            $this->load->view('admin/contact',$data);
        }
    }

    /*********save Privacy Policy********/

    function savePolicy(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_POST);die;
            $id = $this->input->post('id');
            $language = $this->input->post('language');
            
            $updateData = array('title'=>$this->input->post('policytitle'),
                'user_id'=>$this->session->userdata('admin_id'),
                'content'=>$this->input->post('policycontent'),
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->savePolicy($updateData,$id,$language);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('policysuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/privacyPolicy');
           // $this->load->view('admin/contact',$data);
        }
    }

    /*********save Terms & Condition********/

    function saveTerms(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_POST);die;
            $id = $this->input->post('id');
            $language = $this->input->post('language');
            
            $updateData = array('title'=>$this->input->post('termstitle'),
                'content'=>$this->input->post('termscontent'),
                'user_id'=>$this->session->userdata('admin_id'),
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->saveTerms($updateData,$id,$language);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('termssuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/termsConditions');
           // $this->load->view('admin/contact',$data);
        }
    }

    /******Jobs ******/
    function jobs(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           $sitelang =  $this->session->userdata('site_lang');
           $user_id=$this->session->userdata('admin_id');
            $data['alljobs']=$this->user_admin->getalljobs($sitelang,$user_id);
            $data['page']='jobs';
            $this->load->view('admin/jobs',$data);
        }
    }

    /*****Add new jobs*****/
    function addjobs(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           $sitelang =  $this->session->userdata('site_lang');
            $user_id=$this->session->userdata('admin_id');
            $data['alljobs']=$this->user_admin->getalljobs($sitelang,$user_id);
            $data['page']='addjobs';
            $this->load->view('admin/addjobs',$data);
        }
    }
    /*** Save Jobs****/
     function savejobs(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
         }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_FILES);
           
            
            $language = $this->input->post('language');

            //Check whether user upload picture
                        if(!empty($_FILES['userimage']['name'])){
                            $config['upload_path'] = 'assets/jobsimage/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['userimage']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('userimage')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }else{
                                $picture = '';
                            }
                        }
            
            $insertData = array('title'=>$this->input->post('jobtitle'),
                'description'=>$this->input->post('jobcontent'),
                'image'=>$picture,
                'language'=>$language,
                'user_id'=>$this->session->userdata('admin_id'),
                'created_at'=>date('y-m-d H:i:s'),
                'updated_at'=>date('y-m-d H:i:s')
                );
             //print_r($insertData);die;
            $isSaved = $this->user_admin->saveJobs($insertData);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('Jobsuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/jobs');
           // $this->load->view('admin/contact',$data);
        }
     }

     /*******Edit jobs********/
     function editjobs($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
       
            $data['getjobs']=$this->user_admin->getjobs($id);
            $data['page']='addjobs';
            $this->load->view('admin/editjobs',$data);
        }
     }

     /********save update jobs*******/

     function updatejobs(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_FILES);
            // print_r($_POST);die;
            $id = $this->input->post('id');
            $language = $this->input->post('language');
             //Check whether user upload picture
                        if(!empty($_FILES['userimage']['name'])){
                            $config['upload_path'] = 'assets/jobsimage/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['userimage']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('userimage')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }else{
                                $picture = $this->input->post('oldimge');
                            }
                        }else{
                            $picture = $this->input->post('oldimge');
                        }
            $updateData = array('title'=>$this->input->post('jobtitle'),
                'description'=>$this->input->post('jobcontent'),
                'image'=>$picture,
                'user_id'=>$this->session->userdata('admin_id'),
                'language'=>$language,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->saveupdatejobs($updateData,$id,$language);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('Jobsuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/jobs');
           // $this->load->view('admin/contact',$data);
        }
    }

    /******delete jobs*******/
    function deletejobs($id){
        if(!empty($id)){
            $delete = $this->user_admin->deletejobs($id);
            $this->session->set_flashdata('success', $this->lang->line('Jobsuccss'));
        }else{
            $this->session->set_flashdata('error', $this->lang->line('error'));
        }

        redirect('admin/jobs');
    }
    /*********Logout********/
    public function logout(){
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('email');
		$this->session->unset_userdata('name');
        $this->session->unset_userdata('logged_in_back');
        $this->session->unset_userdata('username');
      //  $this->session->unset_userdata('site_lang');
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('admin','refresh');
    }


     /*****save Contact*****/

    function saveContact(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_FILES);
            // print_r($_POST);die;
            $id = $this->input->post('id');
            $language = $this->input->post('language');
             //Check whether user upload picture
                        if(!empty($_FILES['userimage']['name'])){
                            $config['upload_path'] = 'assets/contact/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['userimage']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('userimage')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }else{
                                $picture = $this->input->post('oldimge');
                            }
                        }else{
                            $picture = $this->input->post('oldimge');
                        }
            $updateData = array('title'=>$this->input->post('contacttitle'),
                'content'=>$this->input->post('contactcontent'),
                'image'=>$picture,
                'user_id'=>$this->session->userdata('admin_id'),
                'language'=>$language,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->saveContact($updateData,$id,$language);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('Contactsuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/contact');
           // $this->load->view('admin/contact',$data);
        }
        
    }

    /********Get users list*******/

    public function users()
    {
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           //  echo $this->session->userdata('site_lang');die;
            $data['userlist']= $this->user_admin->getallusers();
            $data['page']='userlist';
            $this->load->view('admin/users',$data);
        }
    }


    /************** Verify chef ****************/
    function chefverify($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{

           //  echo $this->session->userdata('site_lang');die;
            $data['chefusers']= $this->user_admin->getchefuser($id);
            $data['page']='userlist';
            $this->load->view('admin/verifychef',$data);
        }
    }
    /********verified chef by admin**********/
    function verifiedchef(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            if(isset($_POST['submit'])){
            $userid = $this->input->post('user_id');
            $email = $this->input->post('email');
            $verifychef = $this->input->post('verifychef');
             $updateData = array('is_verify_chef'=> $verifychef,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->Verfiychef($updateData,$userid);
            if($isSaved){

                /******Send mail to chef******/
                                // $message = 'Hello User ,';
                                // $config['mailtype'] = 'html';
                                // $config['charset']  = 'iso-8859-1';
                                // $config['wordwrap'] = TRUE;
                                // $config['newline']  = "\r\n"; 
                                // $this->load->library('email', $config);
                                // $this->load->helper('path');

                                // $this->email->from('test@vervelogic.com', 'Salestrackerr');
                                // $this->email->to($this->input->post('customer_email')); 
                                // // $this->email->cc('another@another-example.com'); 
                                // // $this->email->bcc('them@their-example.com'); 

                                // $this->email->subject('You are verify by Admin');
                                // $this->email->message($message); 
                                // $mail = $this->email->send();
                 $this->session->set_flashdata('success', $this->lang->line('chefverifysuccess'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }
            
        }
         redirect('admin/users');
        }
    }

    /*******verfiy users*******/
    function userverify($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{

           //  echo $this->session->userdata('site_lang');die;
            $data['getusers']= $this->user_admin->getusers($id);
            $data['page']='userlist';
            $this->load->view('admin/verifyusers',$data);
        }
        
    }

    /**********verifiedusers********/
    function verifiedusers(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            if(isset($_POST['submit'])){
            $userid = $this->input->post('user_id');
            $email = $this->input->post('email');
            $verifyuser = $this->input->post('verifyuser');
             $updateData = array('is_verify'=> $verifyuser,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->VerfiyUsers($updateData,$userid);
            if($isSaved){

                /******Send mail to chef******/
                                // $message = 'Hello User ,';
                                // $config['mailtype'] = 'html';
                                // $config['charset']  = 'iso-8859-1';
                                // $config['wordwrap'] = TRUE;
                                // $config['newline']  = "\r\n"; 
                                // $this->load->library('email', $config);
                                // $this->load->helper('path');

                                // $this->email->from('test@vervelogic.com', 'Salestrackerr');
                                // $this->email->to($this->input->post('customer_email')); 
                                // // $this->email->cc('another@another-example.com'); 
                                // // $this->email->bcc('them@their-example.com'); 

                                // $this->email->subject('You are verify by Admin');
                                // $this->email->message($message); 
                                // $mail = $this->email->send();

                 $this->session->set_flashdata('success', $this->lang->line('userverifysuccess'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }
            
        }
         redirect('admin/users');
        }
    }

    /*****Get menu list****/

    public function menus()
    {
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['menulist']= $this->user_admin->getallList();
            $data['page']='menulist';
            $this->load->view('admin/menus',$data);
        }
    }

    /******Get submenu list*******/

    public function submenu($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['submenulist']= $this->user_admin->getallsubmenu($id);
            $data['page']='menulist';
            $this->load->view('admin/submenus',$data);
        }
    }

    /*******Edit menus********/

    public function Editmenu($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['menudata']= $this->user_admin->getmenudata($id);
            $data['page']='menulist';
            $this->load->view('admin/editmenus',$data);
        }
    }

    /******Save edit menu****/

    function savemenus(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            if(isset($_POST['submit'])){
            $menuid = $this->input->post('menu_id');
            $status = $this->input->post('status');
             $updateData = array('status'=> $status,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->Savemenu($updateData,$menuid);
            if($isSaved){

                
                 $this->session->set_flashdata('success', $this->lang->line('menusuccess'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }
            
        }
         redirect('admin/menus');
        }
    }


    /******Edit sub menu*****/

    function editsubmenu($id){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['submenudata']= $this->user_admin->getsubmenudata($id);
            $data['page']='menulist';
            $this->load->view('admin/editsubmenu',$data);
        }
    }

    /*****Save sub menu******/

    function savesubmenus(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            if(isset($_POST['submit'])){
            $submenu_id = $this->input->post('submenu_id');
            $menu_id = $this->input->post('menu_id');
            $status = $this->input->post('status');
             $updateData = array('status'=> $status,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->SaveSubmenu($updateData,$submenu_id);
            if($isSaved){

                
                 $this->session->set_flashdata('success', $this->lang->line('submenusuccess'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }
            
        }
         redirect('admin/submenu/'.$menu_id);
        }
    }
/*******Delete menus********/
    function deleteMenus($menuid){
          if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            
            $isSaved = $this->user_admin->deleteMenus($menuid);
            if($isSaved){

                
                 $this->session->set_flashdata('success', $this->lang->line('deletemenus'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

            redirect('admin/menus');
        }
    }
/********Delete submenus*************/
    function deletesubMenus($submenuid,$menuid){
          if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
        // /  echo $menuid;die;
            $isSaved = $this->user_admin->deletesubMenus($submenuid);
            if($isSaved){

                
                 $this->session->set_flashdata('success', $this->lang->line('deletesubmenus'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

            redirect('admin/submenu/'.$menuid);
        }
    }
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */