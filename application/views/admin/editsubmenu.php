<?php
  $this->load->view('admin/header');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
 <div id="content-header">
  <div id="breadcrumb"> <a href="<?php echo base_url();?>index.php/admin/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> <?php echo $this->lang->line('home'); ?></a> <a href="#" class="current"><?php echo $this->lang->line('editsubmenu'); ?></a> </div>
  <h1><?php echo $this->lang->line('editsubmenu'); ?></h1>
</div>


<div class="container-fluid">
  <hr>
  <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
       
<!--End-breadcrumbs-->
<div class="row-fluid">
<div class="span12">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5><?php echo $this->lang->line('editsubmenu'); ?></h5>
        </div>
        <div class="widget-content">
          <form action="<?php echo base_url();?>index.php/admin/savesubmenus" method="post" class="form-horizontal" name="jobForm" id="jobForm" novalidate="novalidate" enctype='multipart/form-data'>
          <input type="hidden" name="submenu_id" value="<?php if(isset($submenudata[0]['id'])){
          echo ucfirst($submenudata[0]['id']);} ?>">
           <input type="hidden" name="menu_id" value="<?php if(isset($submenudata[0]['menu_id'])){
          echo ucfirst($submenudata[0]['menu_id']);} ?>">

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('dishname'); ?></b></label>
          <input type="text" class="span8 m-wrap"  name="jobtitle" value="<?php if(isset($submenudata[0]['dish_name'])){
          echo ucfirst($submenudata[0]['dish_name']);} ?>" disabled />
        </div> 

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('dishimage'); ?></b></label>
         <img src="<?php echo base_url()?>assets/submenuImage/<?php echo $submenudata[0]['dish_image']?>" width="100px" height="100px"/>
         </div>


        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('dishcategory'); ?></b></label>
          <input type="text" class="span8 m-wrap"  name="jobtitle" value="<?php if(isset($submenudata[0]['dish_category'])){
          echo ucfirst($submenudata[0]['dish_category']);} ?>" disabled />
        </div>

        
       
        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('status'); ?></b></label>
          <select class="span8 m-wrap" name="status">
            <option value="1" <?php if(isset($submenudata[0]['status'] ) && $submenudata[0]['status'] =='1'){
          echo "selected";} ?>><?php echo $this->lang->line('active'); ?></option>
            <option value="0" <?php if(isset($submenudata[0]['status'] ) && $submenudata[0]['status'] =='0'){
          echo "selected";} ?>><?php echo $this->lang->line('deactive'); ?></option>
          </select>
          
        </div>
        


            <div class="form-actions">
              <button type="submit" name="submit" class="btn btn-success"><?php echo $this->lang->line('save'); ?></button>
            </div>
          </form>
        </div>
      </div>
  </div>
  </div>
</div>
</div>

<!--end-main-container-part-->

<?php
  $this->load->view('admin/footer');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
var lang = '<?php echo $this->session->userdata('site_lang') ?>';
//alert(lang);
var code;
if(lang !='' && lang =='simplified-chinese'){
 // alert('1');
code ='zh-CN';
}else if(lang !='' && lang =='traditional-chinese'){
//  alert('2');
code ='zh-TW';
}else{
  //alert('3');
  code ='en';
}
CKEDITOR.replace( 'jobcontent', {
      language: code
    } );
// var editor = CKEDITOR.replace("policycontent");
// editor.extraPlugins = 'language';
</script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    // alert('hiiii');
// Form Validation
var lang  = "<?php echo $this->session->userdata('site_lang');?>";
var titlemsg,contentmsg,img;
if(lang !='' && lang !='null'){
  lang =lang;
}else{
  lang = 'english';
}
//alert(lang);
if(lang == 'simplified-chinese'){
titlemsg = '请输入标题。';
contentmsg = '请输入内容。';
img = '请，上传图片';
}else if(lang =='traditional-chinese'){
titlemsg = '請輸入標題。';
contentmsg = '請輸入內容。';
img='請，上傳圖片'
}else{
titlemsg = 'Please, Enter the title.';
contentmsg='Please, Enter the content.';
img='Please,Upload image';
}
    $("#jobForm").validate({
     rules:{
      "jobtitle":{
        required:true
      },
      "jobcontent":{
        required:true
      },
      "userimage":{
        required:true
      }
      // url:{
      //   required:true,
      //   url: true
      // }
    },
     messages: {
            "jobtitle": {
                required: titlemsg
            },
            "jobcontent": {
                required: contentmsg
            },
            "userimage": {
                required: img
            }

        },
    errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
      $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element).parents('.control-group').removeClass('error');
      $(element).parents('.control-group').addClass('success');

       //alert('hiiii');
    }
  });
   });
</script>