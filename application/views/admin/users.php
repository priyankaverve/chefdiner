<?php
  $this->load->view('admin/header');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url();?>index.php/admin/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> <?php echo $this->lang->line('home'); ?></a> <a href="users" class="current"><?php echo $this->lang->line('user'); ?></a> </div>
    <h1><?php echo $this->lang->line('userlist'); ?></h1>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
<hr>
  <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>

       <?php 
        $langu =  $this->session->userdata('site_lang');
          if($langu !=''){
            $langu =  $this->session->userdata('site_lang');
          }else{
            $langu = 'english';
          }
      ?>

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>All <?php echo $this->lang->line('userlist') ?></h5>
            
          
          </div>
         
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th><?php echo $this->lang->line('serialno') ?></th>
                  <th><?php echo $this->lang->line('usersName') ?></th>
                  <th><?php echo $this->lang->line('usersEmail') ?></th>
                  <th><?php echo $this->lang->line('isuser') ?></th>
                  <th><?php echo $this->lang->line('ischef') ?></th>
                  <th><?php echo $this->lang->line('isuserverify') ?></th>
                  <th><?php echo $this->lang->line('ischefverify') ?></th>
                </tr>
              </thead>
              <tbody>
              <?php 
              if(!empty($userlist)){
                $i=1;
                foreach ($userlist as $key => $value) { 
                  
                  ?>
                <tr class="gradeX" align="center">
                  <td><?php echo $i; ?></td>
                  <td><?php echo ucfirst($value['first_name'].' '.$value['last_name']); ?></td>
                  <td><?php echo $value['email']; ?></td>
                  <td><?php if($value['is_user'] =='1'){
                    $user = $this->lang->line('yes');
                    ?>
                     <button class="btn btn-success btn-mini"><?=$user?></button>
                    <?php
                  }else{
                    $user = $this->lang->line('no');
                    ?>
                     <button class="btn btn-danger btn-mini"><?=$user?></button>
                    <?php
                  }?></td>
                  <td><?php if($value['is_chef'] =='1'){
                    $chef = $this->lang->line('yes');
                    ?>
                     <button class="btn btn-success btn-mini"><?=$chef?></button>
                    <?php
                  }else{
                    $chef = $this->lang->line('no');
                    ?>
                     <button class="btn btn-danger btn-mini"><?=$chef?></button>
                    <?php
                  }?></td>
                  <td>
                  <?php if($value['is_verify'] =='yes'){
                    $verifyuser = $this->lang->line('yes');
                    ?>
                     <button class="btn btn-success btn-mini"><?=$verifyuser?></button>

                 <?php }else{
                    $verifyuser = $this->lang->line('no');
                    ?>
                    <button class="btn btn-danger btn-mini"><?=$verifyuser?></button>
                     <a href="<?php echo base_url();?>index.php/admin/userverify/<?php echo $value['id']; ?>" lass="btn btn-success btn-mini"><i class=" icon-pencil"></i></a>
                    <?php
                  }
                   ?>
                
                  </td>
                  <td>

                  <?php if($value['is_verify_chef'] =='yes'){
                    $verifychef = $this->lang->line('yes');
                    ?>
                     <button class="btn btn-success btn-mini"><?=$verifychef?></button>

                 <?php }else{
                    $verifychef = $this->lang->line('no');
                    ?>
                    <button class="btn btn-danger btn-mini"><?=$verifychef?></button>
                    <?php
                  }
                   ?>
                  <?php if($value['is_chef'] =='1'){ ?>
                  <a href="<?php echo base_url();?>index.php/admin/chefverify/<?php echo $value['id']; ?>" lass="btn btn-success btn-mini"><i class="icon-pencil"></i></a> </td>
                  <?php
                }else{ ?>

                  <a href="#" lass="btn btn-success btn-mini" disabled><i class="icon-remove-circle"></i></a> </td>
               <?php }


                    ?>
                  
                  
                </tr>
          <?php  
            $i++;

              }

              }
              ?>
               
               
              </tbody>
            </table>
          </div>
        </div>
</div>

<!--end-main-container-part-->

<?php
  $this->load->view('admin/footer');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 
<script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.uniform.js"></script> 
<script src="<?php echo base_url();?>assets/js/matrix.tables.js"></script> 
<script type="text/javascript">
  
  function confirmDialog(lan) {
   // alert(lan);
    if(lan =='simplified-chinese'){
      return confirm("您确定要删除此记录吗？")
    }else if(lan =='traditional-chinese'){
      return confirm("您確定要刪除此記錄嗎？")
    }else{
      return confirm("Are you sure you want to delete this record?")
    }
    
  }


</script>