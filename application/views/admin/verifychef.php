<?php
  $this->load->view('admin/header');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
 <div id="content-header">
  <div id="breadcrumb"> <a href="<?php echo base_url();?>index.php/admin/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> <?php echo $this->lang->line('home'); ?></a> <a href="#" class="current"><?php echo $this->lang->line('verifychef'); ?></a> </div>
  <h1><?php echo $this->lang->line('verifychef'); ?></h1>
</div>


<div class="container-fluid">
  <hr>
  <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
       
<!--End-breadcrumbs-->
<div class="row-fluid">
<div class="span12">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5><?php echo $this->lang->line('verifychef'); ?></h5>
        </div>
        <div class="widget-content">
          <form action="<?php echo base_url();?>index.php/admin/verifiedchef" method="post" class="form-horizontal" name="jobForm" id="jobForm" novalidate="novalidate" enctype='multipart/form-data'>
          <input type="hidden" name="user_id" value="<?php if(isset($chefusers[0]['user_id'])){
          echo ucfirst($chefusers[0]['user_id']);} ?>">

          <input type="hidden" name="email" value="<?php if(isset($chefusers[0]['email'])){
          echo ucfirst($chefusers[0]['email']);} ?>">

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('uploadpic'); ?></b></label>
          <img src="<?php echo base_url()?>assets/chefprofile/<?php  if(isset($chefusers[0]['profile_pic'])){
          echo ucfirst($chefusers[0]['profile_pic']);} ?>" width="100px" height="100px">
        </div>
        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('preferlanguage'); ?></b></label>
          <input type="text" class="span8 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['language'])){
          echo ucfirst($chefusers[0]['language']);} ?>" disabled />
        </div>
        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('nameque'); ?></b></label>
          <input type="text" class="span4 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['chef_fname'])){
          echo ucfirst($chefusers[0]['chef_fname']);} ?>" disabled />
          <input type="text" class="span4 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['chef_lname'])){
          echo ucfirst($chefusers[0]['chef_lname']);} ?>" disabled />
        </div>  

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('speaklanguage'); ?></b></label>
          <input type="text" class="span8 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['language_speak'])){
          echo ucfirst($chefusers[0]['language_speak']);} ?>" disabled />
        </div>

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('phonenum'); ?></b></label>
          <input type="text" class="span8 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['phone_number'])){
          echo ucfirst($chefusers[0]['phone_number']);} ?>" disabled />
        </div>

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('oftencook'); ?></b></label>
          <input type="text" class="span8 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['oftencook'])){
          echo ucfirst($chefusers[0]['oftencook']);} ?>" disabled />
        </div>

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('palcelocated'); ?></b></label>
          <input type="text" class="span4 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['chef_country'])){
          echo ucfirst($chefusers[0]['chef_country']);} ?>" disabled />
          <input type="text" class="span4 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['chef_city'])){
          echo ucfirst($chefusers[0]['chef_city']);} ?>" disabled />
        </div>

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('numberPeople'); ?></b></label>
          <input type="text" class="span4 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['from_guest'])){
          echo ucfirst($chefusers[0]['from_guest']);} ?>" disabled />
          <input type="text" class="span4 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['upto_guest'])){
          echo ucfirst($chefusers[0]['upto_guest']);} ?>" disabled />
        </div>

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('kitchenTitle'); ?></b></label>
          <input type="text" class="span8 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['kitchen_title'])){
          echo ucfirst($chefusers[0]['kitchen_title']);} ?>" disabled />
        </div>

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('kitchenDes'); ?></b></label>
          <input type="text" class="span8 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['kitchen_descrition'])){
          echo ucfirst($chefusers[0]['kitchen_descrition']);} ?>" disabled />
        </div>

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('servicetype'); ?></b></label>
          <input type="text" class="span8 m-wrap"  name="jobtitle" value="<?php if(isset($chefusers[0]['service_type'])){
          echo ucfirst($chefusers[0]['service_type']);} ?>" disabled />
        </div>

        <div class="controls">
          <label class="span12 m-wrap"><b><?php echo $this->lang->line('verifychef'); ?></b></label>
          <select class="span8 m-wrap" name="verifychef">
            <option value="yes" <?php if(isset($chefusers[0]['is_verify_chef'] ) && $chefusers[0]['is_verify_chef'] =='yes'){
          echo "selected";} ?>><?php echo $this->lang->line('yes'); ?></option>
            <option value="no" <?php if(isset($chefusers[0]['is_verify_chef'] ) && $chefusers[0]['is_verify_chef'] =='no'){
          echo "selected";} ?>><?php echo $this->lang->line('no'); ?></option>
          </select>
          
        </div>
        


            <div class="form-actions">
              <button type="submit" name="submit" class="btn btn-success"><?php echo $this->lang->line('save'); ?></button>
            </div>
          </form>
        </div>
      </div>
  </div>
  </div>
</div>
</div>

<!--end-main-container-part-->

<?php
  $this->load->view('admin/footer');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
var lang = '<?php echo $this->session->userdata('site_lang') ?>';
//alert(lang);
var code;
if(lang !='' && lang =='simplified-chinese'){
 // alert('1');
code ='zh-CN';
}else if(lang !='' && lang =='traditional-chinese'){
//  alert('2');
code ='zh-TW';
}else{
  //alert('3');
  code ='en';
}
CKEDITOR.replace( 'jobcontent', {
      language: code
    } );
// var editor = CKEDITOR.replace("policycontent");
// editor.extraPlugins = 'language';
</script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    // alert('hiiii');
// Form Validation
var lang  = "<?php echo $this->session->userdata('site_lang');?>";
var titlemsg,contentmsg,img;
if(lang !='' && lang !='null'){
  lang =lang;
}else{
  lang = 'english';
}
//alert(lang);
if(lang == 'simplified-chinese'){
titlemsg = '请输入标题。';
contentmsg = '请输入内容。';
img = '请，上传图片';
}else if(lang =='traditional-chinese'){
titlemsg = '請輸入標題。';
contentmsg = '請輸入內容。';
img='請，上傳圖片'
}else{
titlemsg = 'Please, Enter the title.';
contentmsg='Please, Enter the content.';
img='Please,Upload image';
}
    $("#jobForm").validate({
     rules:{
      "jobtitle":{
        required:true
      },
      "jobcontent":{
        required:true
      },
      "userimage":{
        required:true
      }
      // url:{
      //   required:true,
      //   url: true
      // }
    },
     messages: {
            "jobtitle": {
                required: titlemsg
            },
            "jobcontent": {
                required: contentmsg
            },
            "userimage": {
                required: img
            }

        },
    errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
      $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element).parents('.control-group').removeClass('error');
      $(element).parents('.control-group').addClass('success');

       //alert('hiiii');
    }
  });
   });
</script>