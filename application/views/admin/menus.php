<?php
  $this->load->view('admin/header');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url();?>index.php/admin/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> <?php echo $this->lang->line('home'); ?></a> <a href="users" class="current"><?php echo $this->lang->line('menu'); ?></a> </div>
    <h1><?php echo $this->lang->line('menulist'); ?></h1>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
<hr>
  <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>

       <?php 
        $langu =  $this->session->userdata('site_lang');
          if($langu !=''){
            $langu =  $this->session->userdata('site_lang');
          }else{
            $langu = 'english';
          }
      ?>

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>All <?php echo $this->lang->line('menulist') ?></h5>
            
          
          </div>
         
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th><?php echo $this->lang->line('serialno') ?></th>
                  <th><?php echo $this->lang->line('chefname')?></th>
                  <th><?php echo $this->lang->line('menutitle')?></th>
                  <th><?php echo $this->lang->line('menuimage')?></th>
                  <th><?php echo $this->lang->line('priceperperson')?></th>
                  <th><?php echo $this->lang->line('currency')?></th>
                  <th><?php echo $this->lang->line('submenu')?></th>
                  <th><?php echo $this->lang->line('addedat')?></th>
                  <th><?php echo $this->lang->line('status')?></th>
                  <th><?php echo $this->lang->line('action')?></th>
                </tr>
              </thead>
              <tbody>
              <?php 
              if(!empty($menulist)){
                $i=1;
                foreach ($menulist as $key => $value) { 
                  if($value['status'] =='1'){
                   $val = '<button class="btn btn-success btn-mini">'.$this->lang->line('active').'</button>';
                  }else{
                     $val = '<button class="btn btn-danger btn-mini">'.$this->lang->line('deactive').'</button>';
                  }
                  
                  ?>
                <tr class="gradeX" align="center">
                  <td><?php echo $i; ?></td>
                  <td><?php echo ucfirst($value['first_name'].' '.$value['last_name']); ?></td>
                  <td><?php echo $value['menu_title']; ?></td>
                  <td><?php if(!empty($value['menu_image'])) {
                    $url = base_url().'assets/menuImgae/'.$value['menu_image'];
                   }else{
                    $url='';
                    } ?>
                   <img src="<?php echo $url; ?>" width="100px" height="100px"></td>
                  <td><?php echo $value['price_per_person']; ?></td>
                  <td><?php echo ucfirst($value['currency']); ?></td>
                  <td> <a href="<?php echo base_url();?>index.php/admin/submenu/<?php echo $value['menu_id']; ?>" class="btn btn-success btn-mini"><?php echo $this->lang->line('view')?></a></td>
                  <td><?php echo $value['created_at'];  ?> </td>
                  <td><?php echo $val;  ?> </td>
                  <td><a href="<?php echo base_url();?>index.php/admin/Editmenu/<?php echo $value['menu_id']; ?>" class="btn btn-success btn-mini"><i class=" icon-pencil"></i></a>
                  <a href="<?php echo base_url();?>index.php/admin/deleteMenus/<?php echo $value['menu_id']; ?>" class="btn btn-danger btn-mini" onclick="return confirmDialog('<?php echo $langu; ?>');"><i class=" icon-remove-circle"></i></a></td>
               
                  
                  
                </tr>
          <?php  
            $i++;

              }

              }
              ?>
               
               
              </tbody>
            </table>
          </div>
        </div>
</div>

<!--end-main-container-part-->

<?php
  $this->load->view('admin/footer');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 
<script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.uniform.js"></script> 
<script src="<?php echo base_url();?>assets/js/matrix.tables.js"></script> 
<script type="text/javascript">
  
  function confirmDialog(lan) {
   // alert(lan);
    if(lan =='simplified-chinese'){
      return confirm("您确定要删除此记录吗？")
    }else if(lan =='traditional-chinese'){
      return confirm("您確定要刪除此記錄嗎？")
    }else{
      return confirm("Are you sure you want to delete this record?")
    }
    
  }


</script>