<?php
  $this->load->view('admin/header');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url();?>index.php/admin/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> <?php echo $this->lang->line('home'); ?></a> <a href="#" class="tip-bottom"><?php echo $this->lang->line('pages'); ?></a> <a href="jobs" class="current"><?php echo $this->lang->line('jobs'); ?></a> </div>
    <h1><?php echo $this->lang->line('jobs'); ?></h1>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
<hr>
  <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>

       <?php 
        $langu =  $this->session->userdata('site_lang');
          if($langu !=''){
            $langu =  $this->session->userdata('site_lang');
          }else{
            $langu = 'english';
          }
      ?>

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>All Jobs list</h5>
             <div align="right">
              <a class="btn btn-success" href="<?php echo base_url() ?>index.php/admin/addjobs"><?php echo $this->lang->line('addjobs'); ?></a>
          </div>
          
          </div>
         
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table"  id= "example2">
              <thead>
                <tr>
                  <th><?php echo $this->lang->line('serialno'); ?></th>
                  <th><?php echo $this->lang->line('jobtitle'); ?></th>
                  <th><?php echo $this->lang->line('image'); ?></th>
                  <th><?php echo $this->lang->line('description'); ?></th>
                  <th><?php echo $this->lang->line('action'); ?></th>
                </tr>
              </thead>
              <tbody>
              <?php 
              if(!empty($alljobs)){
                $i=1;
                foreach ($alljobs as $key => $value) { 
                  // strip tags to avoid breaking any html
                  $string = strip_tags($value['description']);

                  if (strlen($string) > 500) {

                      // truncate string
                      $stringCut = substr($string, 0, 500);

                      // make sure it ends in a word so assassinate doesn't become ass...

                      $url = base_url().'index.php/admin/editjobs/'.$value['id'];
                      $url1 = base_url().'index.php/admin/deletejobs/'.$value['id'];
                      $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="'.$url.'">Read More</a>'; 
                  }
                if(!empty($value['image'])){
                  $img = '<img src="'.base_url().'assets/jobsimage/'.$value['image'].'" width="65px" height="65px" />';
                }
               
                  //echo $string;
                  ?>
                <tr class="gradeX">
                  <td><?php echo $i; ?></td>
                  <td><?php echo $value['title']; ?></td>
                  <td><?php echo $img; ?></td>
                  <td><?php echo $string; ?></td>
                  <td class="center">
                     <a href="<?php echo $url ?>" class="tip-top" data-original-title="Update"><i class="icon-ok"></i></a> 
                     <a href="<?php echo $url1 ?>" class="tip-top" data-original-title="Delete" 
                     onclick="return confirmDialog('<?php echo $langu; ?>');"><i class="icon-remove"></i></a>
                  </td>
                </tr>
          <?php   
          $i++;
             }

              }
              ?>
               
               
              </tbody>
            </table>
          </div>
        </div>
</div>

<!--end-main-container-part-->

<?php
  $this->load->view('admin/footer');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 
<script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.uniform.js"></script> 
<script src="<?php echo base_url();?>assets/js/matrix.tables.js"></script> 
<script type="text/javascript">
  
  function confirmDialog(lan) {
   // alert(lan);
    if(lan =='simplified-chinese'){
      return confirm("您确定要删除此记录吗？")
    }else if(lan =='traditional-chinese'){
      return confirm("您確定要刪除此記錄嗎？")
    }else{
      return confirm("Are you sure you want to delete this record?")
    }
    
  }
</script>