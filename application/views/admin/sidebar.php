<!--sidebar-menu-->
<div id="sidebar">
<?php 
// Define a default Page
  $pg = isset($page) && $page != '' ?  $page :'admindash'  ;    
?>
<a href="#" class="visible-phone"><i class="icon icon-home"></i> <?php echo $this->lang->line('dashboard'); ?></a>
  <ul>
    <li <?php echo  $pg =='admindash' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>index.php/admin/main"><i class="icon icon-home"></i> <span><?php echo $this->lang->line('dashboard'); ?></span></a> </li>
    <!-- <li> <a href="charts.html"><i class="icon icon-signal"></i> <span>Charts &amp; graphs</span></a> </li>
    <li> <a href="widgets.html"><i class="icon icon-inbox"></i> <span>Widgets</span></a> </li>
    <li><a href="tables.html"><i class="icon icon-th"></i> <span>Tables</span></a></li>
    <li><a href="grid.html"><i class="icon icon-fullscreen"></i> <span>Full width</span></a></li> -->
    <li <?php if($pg=='privacypolicy' || $pg=='termsconditions' || $pg =='contact'){  echo 'class="submenu active"';}else{ echo 'class="submenu"';} ?>> <a href="#"><i class="icon icon-th-list"></i> <span><?php echo $this->lang->line('pages'); ?></span> <span class="label label-important">4</span></a>
      <ul>
        <li <?php echo  $pg =='privacypolicy' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>index.php/admin/privacyPolicy"><?php echo $this->lang->line('privacy'); ?></a></li>
        <li <?php echo  $pg =='termsconditions' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>index.php/admin/termsConditions"><?php echo $this->lang->line('termscondition'); ?></a></li>
        <li <?php echo  $pg =='contact' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>index.php/admin/contact "><?php echo $this->lang->line('contact'); ?></a></li>
        <li <?php if($pg=='jobs' || $pg=='addjobs'){  echo 'class="active"';}else{ echo 'class=""';}?>><a href="<?php echo base_url();?>index.php/admin/jobs "><?php echo $this->lang->line('jobs'); ?></a></li>
      </ul>
    </li>

    <li <?php echo  $pg =='userlist' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>index.php/admin/users"><i class="icon-group"></i> <span><?php echo $this->lang->line('user'); ?></span></a> </li>

     <li <?php echo  $pg =='menulist' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>index.php/admin/menus"><i class="icon icon-th-list"></i> <span><?php echo $this->lang->line('menu'); ?></span></a> </li>
    
    <!-- <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Addons</span> <span class="label label-important">5</span></a>
      <ul>
        <li><a href="index2.html">Dashboard2</a></li>
        <li><a href="gallery.html">Gallery</a></li>
        <li><a href="calendar.html">Calendar</a></li>
        <li><a href="invoice.html">Invoice</a></li>
        <li><a href="chat.html">Chat option</a></li>
      </ul>
    </li> -->
  <!--<li><a href="buttons.html"><i class="icon icon-tint"></i> <span>Buttons &amp; icons</span></a></li>
    <li><a href="interface.html"><i class="icon icon-pencil"></i> <span>Eelements</span></a></li>
     <li class="submenu"> <a href="#"><i class="icon icon-info-sign"></i> <span>Error</span> <span class="label label-important">4</span></a>
      <ul>
        <li><a href="error403.html">Error 403</a></li>
        <li><a href="error404.html">Error 404</a></li>
        <li><a href="error405.html">Error 405</a></li>
        <li><a href="error500.html">Error 500</a></li>
      </ul>
    </li>
    <li class="content"> <span>Monthly Bandwidth Transfer</span>
      <div class="progress progress-mini progress-danger active progress-striped">
        <div style="width: 77%;" class="bar"></div>
      </div>
      <span class="percent">77%</span>
      <div class="stat">21419.94 / 14000 MB</div>
    </li>
    <li class="content"> <span>Disk Space Usage</span>
      <div class="progress progress-mini active progress-striped">
        <div style="width: 87%;" class="bar"></div>
      </div>
      <span class="percent">87%</span>
      <div class="stat">604.44 / 4000 MB</div>
    </li> -->
  </ul>
</div>
<!--sidebar-menu-->
