<?php
  $this->load->view('admin/header');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
 <div id="content-header">
  <div id="breadcrumb"> <a href="<?php echo base_url();?>index.php/admin/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> <?php echo $this->lang->line('home'); ?></a> <a href="#" class="tip-bottom"><?php echo $this->lang->line('pages'); ?></a> <a href="contact" class="current"><?php echo $this->lang->line('contact'); ?></a> </div>
  <h1><?php echo $this->lang->line('contact'); ?></h1>
</div>


<div class="container-fluid">
  <hr>
  <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <?php 
        $langu =  $this->session->userdata('site_lang');
          if($langu !=''){
            $langu =  $this->session->userdata('site_lang');
          }else{
            $langu = 'english';
          }
      ?>
<!--End-breadcrumbs-->
<div class="row-fluid">
<div class="span12">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5><?php echo $this->lang->line('contact'); ?></h5>
        </div>
        <div class="widget-content nopadding">
          <form action="<?php echo base_url();?>index.php/admin/saveContact" method="post" class="form-horizontal" name="validateForm" id="validateForm" novalidate="novalidate" enctype='multipart/form-data'>
          <input type="hidden" name="id" value="<?php if(isset($getcontact[0]['id'])){ echo $getcontact[0]['id'];} ?>">
          <input type="hidden" name="language" value="<?php echo $langu;?>">
            <div class="control-group">
              <label class="control-label"><?php echo $this->lang->line('title'); ?> :</label>
              <div class="controls">
                <input type="text" class="span11"  name="contacttitle" placeholder="<?php echo $this->lang->line('title').'..'; ?>" value="<?php if(isset($getcontact[0]['title'])){ echo $getcontact[0]['title'];} ?>"/>
              </div>
            </div>
           <div class="control-group">
              <label class="control-label"><?php echo $this->lang->line('image'); ?> :</label>
              <div class="controls">
              <input type="hidden" name="oldimge" value="<?php if(isset($getcontact[0]['image']) && !empty($getcontact[0]['image'])){ echo $getcontact[0]['image'];} ?>">
                <input type="file" class="span11"  name="userimage"  />

                <?php
                if(!empty($getcontact[0]['image'])){
                  $img = '<img src="'.base_url().'assets/contact/'.$getcontact[0]['image'].'" width="65px" height="65px" />';
                }
                echo $img;
                 ?>
              </div>
            </div>
           
            <div class="control-group">
              <label class="control-label"><?php echo $this->lang->line('content'); ?></label>
            <div class="controls">
                <textarea class="span11" id="contactcontent" name="contactcontent"><?php if(isset($getcontact[0]['content'])){ echo $getcontact[0]['content'];} ?></textarea>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" name="submit" class="btn btn-success"><?php echo $this->lang->line('save'); ?></button>
            </div>
          </form>
        </div>
      </div>
  </div>
  </div>
</div>
</div>

<!--end-main-container-part-->

<?php
  $this->load->view('admin/footer');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
var lang = '<?php echo $this->session->userdata('site_lang') ?>';
//alert(lang);
var code;
if(lang !='' && lang =='simplified-chinese'){
 // alert('1');
code ='zh-CN';
}else if(lang !='' && lang =='traditional-chinese'){
//  alert('2');
code ='zh-TW';
}else{
  //alert('3');
  code ='en';
}
CKEDITOR.replace( 'contactcontent', {
      language: code
    } );
// var editor = CKEDITOR.replace("policycontent");
// editor.extraPlugins = 'language';
</script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    // alert('hiiii');
// Form Validation
var lang  = "<?php echo $this->session->userdata('site_lang');?>";
var titlemsg,contentmsg;
if(lang !='' && lang !='null'){
  lang =lang;
}else{
  lang = 'english';
}
//alert(lang);
if(lang == 'simplified-chinese'){
titlemsg = '请输入标题。';
contentmsg = '请输入内容。';
}else if(lang =='traditional-chinese'){
titlemsg = '請輸入標題。';
contentmsg = '請輸入內容。';
}else{
titlemsg = 'Please, Enter the title.';
contentmsg='Please, Enter the content.';
}
    $("#validateForm").validate({
     rules:{
      "contacttitle":{
        required:true
      },
      "contactcontent":{
        required:true,
      },
      // date:{
      //   required:true,
      //   date: true
      // },
      // url:{
      //   required:true,
      //   url: true
      // }
    },
     messages: {
            "contacttitle": {
                required: titlemsg
            },
            "contactcontent": {
                required: contentmsg
            }

        },
    errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
      $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element).parents('.control-group').removeClass('error');
      $(element).parents('.control-group').addClass('success');

       //alert('hiiii');
    }
  });
   });
</script>