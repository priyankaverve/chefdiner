<!DOCTYPE html>
<html lang="en">
    
<head>
        <title><?php echo $this->lang->line('sitetitle'); ?></title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-login.css" />
        <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>


<p><?php //echo $this->lang->line('welcome_message'); ?></p>
 <div id="loginbox">
  <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
<select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/admin/switchLang/'+this.value;">
    <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English</option>
    <option value="simplified-chinese" <?php if($this->session->userdata('site_lang') == 'simplified-chinese') echo 'selected="selected"'; ?>>Simplified Chinese</option>
    <option value="traditional-chinese" <?php if($this->session->userdata('site_lang') == 'traditional-chinese') echo 'selected="selected"'; ?>>Traditional Chinese</option>   
</select> 
   </div>

 </div>

</div>

            <form id="loginform" class="form-vertical" action="<?php echo base_url();?>index.php/admin/login" method="post">
				 <div class="control-group normal_text"> <h3><img src="<?php echo base_url();?>assets/img/logo.png" style="width:300px;" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" name="username" placeholder="<?php echo $this->lang->line('username'); ?>" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" name="password" placeholder="<?php echo $this->lang->line('password'); ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover"><?php echo $this->lang->line('lost_password'); ?></a></span>
                    <span class="pull-right"><input type="submit" name="submit" value="<?php echo $this->lang->line('login'); ?>"  class="btn btn-success"></span>
                </div>
            </form>
            <form id="recoverform" action="#" class="form-vertical">
				<p class="normal_text"><?php echo $this->lang->line('lost_password_msg'); ?></p>
				
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input type="text" placeholder="<?php echo $this->lang->line('emailadd');?>" />
                        </div>
                    </div>
               
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; <?php echo $this->lang->line('backtologin') ?></a></span>
                    <span class="pull-right"><a class="btn btn-info"/><?php echo $this->lang->line('rcover') ?></a></span>
                </div>
            </form>
        </div>
        
        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>  
        <script src="<?php echo base_url();?>assets/js/matrix.login.js"></script> 
    </body>

</html>
