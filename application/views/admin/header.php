<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/fullcalendar.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-style.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-media.css" />
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.gritter.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>
<style type="text/css">
  .logo{
    max-width: 200px;
    width: 100%;
  }
  .logo img{
 max-width: 100%;
  }
</style>
<!--Header-part-->
<div id="header">
<div class="logo"><img src="<?php echo base_url();?>assets/img/logo.png" style="width:200px;" alt="Logo" /></div>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text"><?php echo $this->lang->line('welcome') ?> <?php echo ucfirst($this->session->userdata('name')); ?></span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="#"><i class="icon-user"></i> <?php echo $this->lang->line('myprofile') ?></a></li>
        <li class="divider"></li>
        <li><a href="#"><i class="icon-check"></i> <?php echo $this->lang->line('mytask') ?></a></li>
        <li class="divider"></li>
        <li><a href="<?php echo base_url();?>index.php/admin/logout"><i class="icon-key"></i> <?php echo $this->lang->line('logout') ?></a></li>
      </ul>
    </li>
   <!--  <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span> <span class="label label-important">5</span> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a class="sAdd" title="" href="#"><i class="icon-plus"></i> new message</a></li>
        <li class="divider"></li>
        <li><a class="sInbox" title="" href="#"><i class="icon-envelope"></i> inbox</a></li>
        <li class="divider"></li>
        <li><a class="sOutbox" title="" href="#"><i class="icon-arrow-up"></i> outbox</a></li>
        <li class="divider"></li>
        <li><a class="sTrash" title="" href="#"><i class="icon-trash"></i> trash</a></li>
      </ul>
    </li> -->
    <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text"><?php echo $this->lang->line('setting') ?></span></a></li>
    <li class=""><a title="" href="<?php echo base_url();?>index.php/admin/logout"><i class="icon icon-share-alt"></i> <span class="text"><?php echo $this->lang->line('logout') ?></span></a></li>
  </ul>


</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
<?php
// echo $this->lang->line('language');die;
// if($this->session->userdata('site_lang') == 'english'){
// $language =$this->lang->line('language');
// }else if($this->session->userdata('site_lang') == 'simplified-chinese'){
// $language =$this->lang->line('language');

// }else if($this->session->userdata('site_lang') == 'traditional-chinese'){
//   $language =$this->lang->line('language');

// }

?>
  <input type="text" value="<?php echo $this->lang->line('language') ?>" disabled/>
  <!-- <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button> -->
</div>
<!--close-top-serch-->


