<?php
class Api_model extends CI_Model {

	public function _generate_token()
	{
		$this->load->helper('security');
		$salt = do_hash(time().mt_rand());
		$new_key = substr($salt, 0, config_item('rest_key_length'));
		return $new_key;
	}

	function userLogin($data){
		$req = $this->db->select('*')->where(array('email'=>$data['email'],'is_verify'=>'yes'))->get('register');
		
		if($req->num_rows() > 0){
			$res = $req->row();
			
			if(md5($data['password']) == $res->password){
				if($res->status =='1'){  //if user is active
					$userid = $res->id;
					$first_name = $res->first_name;
					//udating user deviceid and auth token
					$updateToken = $this->updateDeviceIdToken($userid,$data['deviceId'],$data['authtoken'],$data['deviceType']);
					if(!$updateToken){
						return FALSE;
					}else{
						$returnData['userId'] = $userid;
						$returnData['first_name'] = $first_name;
						$returnData['last_name'] = $res->last_name;
						//$returnData['authtoken'] = $data['authtoken'];
						$returnData['is_chef'] = $res->is_chef;
						$returnData['is_verify_chef'] = $res->is_verify_chef;
						$returnData['phone_number'] = $res->phone_number;
						$returnData['email'] = $res->email;
						$returnData['referal_code'] = $res->referal_code;
						if($res->facebook_login == 'yes' && $res->facebook_login !=''){
							if(empty($res->profile_pic)){
							$res->profile_pic = '';
							} else {
							$res->profile_pic = $res->profile_pic;
							}
						}else{
							if(empty($res->profile_pic)){
							$res->profile_pic = '';
							} else {
							$res->profile_pic = base_url().'assets/chefprofile/'.$res->profile_pic;
							}
						}
						
						$returnData['profile_pic'] = $res->profile_pic;
						return $returnData;
						//return TRUE;
					}
					
				} else {
					return 'NA'; //not active
				}
			} else {
				return 'WP'; //wrong password
			}
		}
		else {
			return 'NA'; //not active
			}
	}

/**
	* Update user deviceid and auth token while login
	*/
    function updateDeviceIdToken($userid,$deviceId,$authToken,$deviceType){
		$req = $this->db->select('id')->where('id',$userid)->get('register');
		if($req->num_rows()){
			$this->db->update('register',array('deviceId'=>$deviceId,'authtoken'=>$authToken,'deviceType'=>$deviceType),array('id'=>$userid));
			return TRUE;
		}
		
		return FALSE;
	}
	function userRegister($data){
		if(!empty($data)){
			/******Check email already exits******/

			$email = $data['email'];
			$query ="SELECT *  from register where email ='$email'";
		    $req= $this->db->query($query);
			if($req->num_rows() > 0){
				return 'AE';// email already exist
			}else{
			$this->db->insert('register',$data);
			return array('id'=> $this->db->insert_id());
			}
			
		}else{
			return 'NR';//not register
		}

	}

	/**
	* Function for check provided token is valid or not
	*/
	function isValidToken($authToken)
	{	
		$this->db->select('*');
		$this->db->where('authtoken',$authToken);
		if($query = $this->db->get('register')){
			if($query->num_rows() > 0){
				//echo "string";die;
				return $query->row();
			}
		}
		
		return FALSE;
	}

	/********Become a chef******/
	function becomeChef($data){
		$userid = $data['user_id'];

		//check user exists in beacomechef table
		$query ="SELECT *  from become_chef where user_id ='$userid'";
		    $req= $this->db->query($query);
			if($req->num_rows() > 0){
				/******Update profile of chef******/
				$chefdetails = $req->result();
				//print_r($chefdetails);
				$chefid = $chefdetails[0]->id;
				$where = array('user_id' => $userid,'id'=>$chefid);
				$isUpdate = $this->db->update('become_chef',$data,$where); // updata on become a chef
				if($isUpdate){
					/***update register table****/
					$wh = array('id' => $userid);
					$updatedata['is_chef']= '1';
					$updatedata['first_name'] =$data['chef_fname'];
					$updatedata['last_name'] =$data['chef_lname'];
					if(array_key_exists('profile_pic', $data)){
						$updatedata['profile_pic'] =$data['profile_pic'];
					}
					
					$updatedata['phone_number'] =$data['phone_number']; 
					$updatedata['updated_at'] = date('Y-m-d H:i:s');
					$this->db->update('register',$updatedata,$wh);


				}
		        return array('user_id' => $userid,'id'=>$chefid);//record update
			}else{
				/*****register user as chef*******/
				$isSaved = $this->db->insert('become_chef',$data);
				$lastinserted = $this->db->insert_id();
				if($isSaved){
					/***update register table****/
					$wh = array('id' => $userid);
					$updatedata['is_chef']= '1';
					$updatedata['first_name'] =$data['chef_fname'];
					$updatedata['last_name'] =$data['chef_lname'];
					if(array_key_exists('profile_pic', $data)){
						$updatedata['profile_pic'] =$data['profile_pic'];
					}
					// $updatedata['profile_pic'] =$data['profile_pic'];
					$updatedata['phone_number'] =$data['phone_number']; 
					$updatedata['updated_at'] = date('Y-m-d H:i:s');
					$this->db->update('register',$updatedata,$wh);


				}

				return array('user_id' => $userid,'id'=>$lastinserted);//record update
			}

	}
	/****get country*****/
	function country(){
	 $returnData = array();
		$req = "SELECT * FROM  `countries`";
		
		$query = $this->db->query($req);
		if($query->num_rows() >0){
		$returnData = $query->result();
	 		}
	 	
		return $returnData;
	}

	/*******get city******/

	function getcity($countryid){
	 $returnData = array();
	 $statesarr = array();
	 if(!empty($countryid)){
	 	/*****get state id by countryid *****/

	 	$req1 = "SELECT * FROM  `states` where country_id = '$countryid'";
	 	$query1 = $this->db->query($req1);
		if($query1->num_rows() >0){
			$states = $query1->result_array();
			foreach ($states as $key => $value) {
			// 	echo "<pre>";
			// print_r($value);
			$statesarr[] = $value['id'];
			}
			// echo "<pre>";
			// print_r($statesarr);

			$allstate = implode(",", $statesarr);
			//echo $allstate;
// die;
			/****Select city***/


		$req = "SELECT * FROM  `cities` where state_id in($allstate)";
	 	$query = $this->db->query($req);
	 	if($query->num_rows() >0){
		$returnData = $query->result();
	 		}
	 	
		return $returnData;
		}

	 }
		
	}

	/********get Chef*******/
	function getChef($latitude,$longitude){
		$idarr = array();
		$returnData = array();
		$req1 = "SELECT * FROM  `register` where is_verify_chef='yes' 
		and is_chef ='1' and is_verify='yes' and is_user = '1'";
		//print_r($req);die;
		$query1 = $this->db->query($req1);
		if($query1->num_rows() >0){
			$users = $query1->result();
			foreach ($users as $key => $value) {
			//print_r($value);die;
			$idarr[] = $value->id;

		}
			//print_r($idarr);
			$ids = implode(",", $idarr);

		// $req = "SELECT * FROM  `become_chef` where user_id in ($ids)";
// $req="SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
//          become_chef.user_id in ($ids)";
			// $req="SELECT *,become_chef.id as cid, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) HAVING distance < 30 ORDER BY distance";
			$req="SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) HAVING distance < 30 ORDER BY distance";
				// $req="SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef  WHERE become_chef.user_id in ($ids) HAVING distance < 30 ORDER BY distance";
//print_r($req);die;
		$query = $this->db->query($req);
		if($query->num_rows() >0){
			$returnData = $query->result();
			foreach($returnData as $val){
			if($val->facebook_login =='yes' && $val->facebook_login !=''){
				if(empty($val->profile_pic)){
				$val->profile_pic = '';
				} else {
				$val->profile_pic = $val->profile_pic;
				}
			}else{
				if(empty($val->profile_pic)){
				$val->profile_pic = '';
				} else {
				$val->profile_pic = base_url().'assets/chefprofile/'.$val->profile_pic;
				}
			}
			

			if(empty($val->image1)){
				$val->image1 = '';
				} else {
				$val->image1 = base_url().'assets/kitchengallery/'.$val->image1;
				}

				if(empty($val->image2)){
				$val->image2 = '';
				} else {
				$val->image2 = base_url().'assets/kitchengallery/'.$val->image2;
				}
				if(empty($val->image3)){
				$val->image3 = '';
				} else {
				$val->image3 = base_url().'assets/kitchengallery/'.$val->image3;
				}
				if(empty($val->image4)){
				$val->image4 = '';
				} else {
				$val->image4 = base_url().'assets/kitchengallery/'.$val->image4;
				}
				if(empty($val->image5)){
				$val->image5 = '';
				} else {
				$val->image5 = base_url().'assets/kitchengallery/'.$val->image5;
				}
				if(empty($val->image6)){
				$val->image6 = '';
				} else {
				$val->image6 = base_url().'assets/kitchengallery/'.$val->image6;
				}
				if(empty($val->image7)){
				$val->image7 = '';
				} else {
				$val->image7 = base_url().'assets/kitchengallery/'.$val->image7;
				}
				if(empty($val->image8)){
				$val->image8 = '';
				} else {
				$val->image8 = base_url().'assets/kitchengallery/'.$val->image8;
				}
				if(empty($val->image9)){
				$val->image9 = '';
				} else {
				$val->image9 = base_url().'assets/kitchengallery/'.$val->image9;
				}
				if(empty($val->image10)){
				$val->image10 = '';
				} else {
				$val->image10 = base_url().'assets/kitchengallery/'.$val->image10;
				}
				if(empty($val->image11)){
				$val->image11 = '';
				} else {
				$val->image11 = base_url().'assets/kitchengallery/'.$val->image11;
				}
				if(empty($val->image12)){
				$val->image12 = '';
				} else {
				$val->image12 = base_url().'assets/kitchengallery/'.$val->image12;
				}
				if(empty($val->image13)){
				$val->image13 = '';
				} else {
				$val->image13 = base_url().'assets/kitchengallery/'.$val->image13;
				}
				if(empty($val->image14)){
				$val->image14 = '';
				} else {
				$val->image14 = base_url().'assets/kitchengallery/'.$val->image14;
				}
				if(empty($val->image15)){
				$val->image15 = '';
				} else {
				$val->image15 = base_url().'assets/kitchengallery/'.$val->image15;
				}
				if(empty($val->image16)){
				$val->image16 = '';
				} else {
				$val->image16 = base_url().'assets/kitchengallery/'.$val->image16;
				}
				if(empty($val->image17)){
				$val->image17 = '';
				} else {
				$val->image17 = base_url().'assets/kitchengallery/'.$val->image17;
				}
				if(empty($val->image18)){
				$val->image18 = '';
				} else {
				$val->image18 = base_url().'assets/kitchengallery/'.$val->image18;
				}
				if(empty($val->image19)){
				$val->image19 = '';
				} else {
				$val->image19 = base_url().'assets/kitchengallery/'.$val->image19;
				}if(empty($val->image20)){
				$val->image20 = '';
				} else {
				$val->image20 = base_url().'assets/kitchengallery/'.$val->image20;
				}

$val->allimage = $val->image1.'|'.$val->image2.'|'.$val->image3.'|'.$val->image4.'|'.$val->image5.'|'.$val->image6.'|'.$val->image7.'|'.$val->image8.'|'.$val->image9.'|'.$val->image10.'|'.$val->image11.'|'.$val->image12.'|'.$val->image13.'|'.$val->image14.'|'.$val->image15.'|'.$val->image16.'|'.$val->image17.'|'.$val->image18.'|'.$val->image19.'|'.$val->image20;

			$cusn = explode(",", $val->cuisine);
		    $cusn = array_values(array_unique(array_filter($cusn),SORT_REGULAR ));
		    if(!empty($cusn)){
		    	$cusns = implode(",", $cusn);
		    	$val->cuisine = $cusns;
		    }else{
		    	$val->cuisine='';
		    }
			}
			return $returnData;
		}else{
			return FALSE;
		}
		}else{
			return FALSE;
		}
	}

/********Add a menu******/
	function addMenu($data){
		$menuid = $data['id'];
	if(!empty($menuid) && $menuid !='0'){
		//check user exists in beacomechef table
		$query ="SELECT * FROM `menus` where id ='$menuid'";
		$req= $this->db->query($query);
			if($req->num_rows() > 0){
				$data['updated_at'] = date('Y-m-d H:i:s');
				/******Update profile of chef******/
				$menudetails = $req->result();
				//print_r($chefdetails);
				$userid = $menudetails[0]->user_id;
				$where = array('user_id' => $userid,'id'=>$menuid);
				$isUpdate = $this->db->update('menus',$data,$where); // updata on become a chef
				/*****Save cuisine in become chef table****/
				
				if($isUpdate){
				$cuisine = $data['cusine'];
				// $where1 = array('user_id' => $userid);
				// $this->db->set('cuisine', 'CONCAT(cuisine,\',\',\''.$cuisine.'\')');
				
				// $this->db->update('become_chef',$cuisine,$where1); // updata on become_chef  table

				$update = "UPDATE become_chef SET cuisine = CONCAT(cuisine, ',$cuisine') WHERE user_id = '$userid'";
				$req1= $this->db->query($update);
// UPDATE become_chef SET cuisine = CONCAT(cuisine, ',Hong Kong Style,Chinese') WHERE user_id = '1'
				}
			
		        return array('user_id' => $userid,'id'=>$menuid);//record update
			}
	}else{
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
				/*****register user as chef*******/
				$isSaved = $this->db->insert('menus',$data);
				$lastinserted = $this->db->insert_id();
				if($isSaved){
					$cuisine = $data['cusine'];
					$userid = $data['user_id'];
				// $where = array('user_id' => $userid);
				// $this->db->set('cuisine', 'CONCAT(cuisine,\',\',\''.$cuisine.'\')', FALSE);
				
				// $this->db->update('become_chef',$cuisine,$where); // updata on become_chef table
					$update = "UPDATE become_chef SET cuisine = CONCAT(cuisine, ',$cuisine') WHERE user_id = '$userid'";
				$req1= $this->db->query($update);

				}
				return array('id'=>$lastinserted);//record insert
			}

	}

	/*******Add/Edit sub menu*******/
	function addSubMenu($data){
		$dishid = $data['id'];
	if(!empty($dishid) && $dishid !='0'){
		//check user exists in beacomechef table
		$query ="SELECT * FROM `sub_menus` where id ='$dishid'";
		$req= $this->db->query($query);
			if($req->num_rows() > 0){
				/******Update profile of chef******/
				$menudetails = $req->result();
				//print_r($chefdetails);
				$userid = $menudetails[0]->user_id;
				$menuid = $menudetails[0]->menu_id;
				$where = array('user_id' => $userid,'id'=>$dishid,'menu_id'=>$menuid);
				$isUpdate = $this->db->update('sub_menus',$data,$where); // updata on become a chef
			
		        return array('user_id' => $userid,'id'=>$dishid);//record update
			}
	}else{
				/*****register user as chef*******/
				$isSaved = $this->db->insert('sub_menus',$data);
				$lastinserted = $this->db->insert_id();
				
				return array('id'=>$lastinserted);//record insert
			}
	}

/*****get menu list*****/
	function getmenutitleprice($menu_id){
		$returnData = array();
		$req = "SELECT * FROM  `menus` where id ='$menu_id'";
		
		$query = $this->db->query($req);
		if($query->num_rows() >0){
		$returnData = $query->result();
			
	 		}
	 	
		return $returnData;
	}
/****get country*****/
	function getMenus($userid){
	 $returnData = array();
		$req = "SELECT * FROM  `menus` where status ='1' and user_id='$userid'";
		
		$query = $this->db->query($req);
		if($query->num_rows() >0){
		$returnData = $query->result();
			foreach($returnData as $val){

			if(empty($val->menu_image)){
			$val->menu_image = '';
			} else {
			$val->menu_image = base_url().'assets/menuImgae/'.$val->menu_image;
			}
			}
	 		}
	 	
		return $returnData;
	}

	/******Get counter of menus********/
	function getcounterdish(){
	 $returnData = array();
		$req = "SELECT * FROM  `sub_menus`";
		
		$query = $this->db->query($req);
		if($query->num_rows() >0){
		$returnData = $query->result();
			}
	 	
		return $returnData;
	}
	/*****get sub menu*****/

	function getSubMenus($menuid){
		$returnData = array();
		$req = "SELECT * FROM  `sub_menus` where menu_id ='$menuid'";
		
		$query = $this->db->query($req);
		if($query->num_rows() >0){
		$returnData = $query->result();
			foreach($returnData as $val){

			if(empty($val->dish_image)){
			$val->dish_image = '';
			} else {
			$val->dish_image = base_url().'assets/submenuImage/'.$val->dish_image;
			}
			}
	 		}
	 	
		return $returnData;
	}

	/********Delete menus********/

	function deleteMenus($menuid){
		if(!empty($menuid)){
			$tablename = 'menus';
			$where = array('id'=>$menuid);
			$this->db->where($where);
          $deleteData =  $this->db->delete($tablename);

          if($deleteData){
          	$tablename1 = 'sub_menus';
			$where1 = array('menu_id'=>$menuid);
			$this->db->where($where1);
          $this->db->delete($tablename1);
          }

         	return TRUE;
		}else{
			return FALSE;
		}

	}

	/******Delete sub menu****/

	function deleteSubmenus($submenu){
		if(!empty($submenu)){
			$tablename = 'sub_menus';
			$where = array('id'=>$submenu);
			$this->db->where($where);
         	$this->db->delete($tablename);
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/******Update user profile******/
	function UpdateuserProfile($data,$userid){

		$where = array('id' => $userid);
		$isUpdate = $this->db->update('register',$data,$where); // updata on user profile
		if($isUpdate){
			//get chef details if user is chef
		$req = "SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
         become_chef.user_id = '$userid' and register.is_chef='1' and register.is_verify_chef='yes' ";
		//print_r($req);die;
		$query = $this->db->query($req);
		if($query->num_rows() >0){
			//update chef table
			$res = $query->row();
			// echo "<pre>";
			// print_r($res);
			$where1 = array('user_id' => $userid);
			$chef['chef_fname']=$res->first_name;
			$chef['chef_lname']=$res->last_name;
			if(array_key_exists('profile_pic', $data)){
				$chef['profile_pic']=$data['profile_pic'];
			}
			
			$chef['updated_at'] = date('Y-m-d H:i:s');
			//print_r($chef);die;
			$isUpdatechef = $this->db->update('become_chef',$chef,$where1);
			return array('id'=>$userid,'user'=>'UC');//user and chef updated successfully
		}
		return array('id'=>$userid,'user'=>'U');//user profile updated successfully
		}else{
			 return array('id'=>$userid,'user'=>'U');//user profile updated successfully
		}

	}
	/***********get user info*************/
	function getuserinfo($userid){
		$returnData = array();
		$req="SELECT * FROM register  WHERE id = '$userid' and is_verify = 'yes'";
         //print_r($req);die;
		$query = $this->db->query($req);
		if($query->num_rows() >0){
			$returnData = $query->result();
			foreach($returnData as $val){
		if($val->facebook_login == 'yes' && $val->facebook_login !=''){
			if(empty($val->profile_pic)){
			$val->profile_pic = '';
			} else {
			$val->profile_pic = $val->profile_pic;
			}
		}else{
			if(empty($val->profile_pic)){
			$val->profile_pic = '';
			} else {
			$val->profile_pic = base_url().'assets/chefprofile/'.$val->profile_pic;
			}
		}
			
			}
			return $returnData;
		}else{
			return FALSE;
		}
	}

	/******Get chef info*******/
function getchefinfo($userid){
		 $returnData = array();
		$req="SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
         become_chef.user_id = '$userid' and register.is_chef='1' and register.is_verify_chef='yes'";
         //print_r($req);die;
		$query = $this->db->query($req);
		if($query->num_rows() >0){
			$returnData = $query->result();
			foreach($returnData as $val){
			if($val->facebook_login == 'yes' && $val->facebook_login !=''){
				if(empty($val->profile_pic)){
				$val->profile_pic = '';
				} else {
				$val->profile_pic = $val->profile_pic;
				}
			}else{
				if(empty($val->profile_pic)){
				$val->profile_pic = '';
				} else {
				$val->profile_pic = base_url().'assets/chefprofile/'.$val->profile_pic;
				}
			}
			
			}
			return $returnData;
		}else{
			return FALSE;
		}
	}

	/********Add a Permission******/
	function addPermission($data){
		$id = $data['id'];
	if(!empty($id) && $id !='0'){
		//check user exists in beacomechef table
		$query ="SELECT * FROM `permission` where id ='$id'";
		$req= $this->db->query($query);
			if($req->num_rows() > 0){
			
				$where = array('id' => $id);
				$isUpdate = $this->db->update('permission',$data,$where); // updata on become a chef
			
		        return array('id'=>$id);//record update
			}
	}else{
				/*****register user as chef*******/
				$isSaved = $this->db->insert('permission',$data);
				$lastinserted = $this->db->insert_id();
				
				return array('id'=>$lastinserted);//record insert
			}

	}

	/*******Get Permission******/
		function getPermission($userid){
		 $returnData = array();
			$req = "SELECT * FROM  `permission` where user_id='$userid'";
			
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 	
		return $returnData;
		}
	/******Kitchen gallery******/
	// function KitchenGallery($data){
	// 	$gid = $data['id'];
	// 	$userid = $data['chef_id'];
	// 	if(!empty($gid) && $gid !='0'){
	// 	//check user exists in beacomechef table
	// 	$query ="SELECT * FROM `kitchen_gallery` where id ='$gid'";
	// 	$req= $this->db->query($query);
	// 		if($req->num_rows() > 0){
			
	// 			$where = array('id' => $gid,'chef_id'=>$userid);
	// 			$data['updated_at'] = date('Y-m-d H:i:s');
	// 			$isUpdate = $this->db->update('kitchen_gallery',$data,$where); // updata on become a chef
			
	// 	        return array('id'=>$gid);//record update
	// 		}
	// }else{
	// 			/*****register user as chef*******/
	// 			$data['created_at'] = date('Y-m-d H:i:s');
	// 			$data['updated_at'] = date('Y-m-d H:i:s');
	// 			$isSaved = $this->db->insert('kitchen_gallery',$data);
	// 			$lastinserted = $this->db->insert_id();
				
	// 			return array('id'=>$lastinserted);//record insert
	// 		}

	// }

	function KitchenGallery($data){
		$gid = $data['id'];
		$userid = $data['user_id'];
		if(!empty($gid) && $gid !='0'){
		//check user exists in beacomechef table
		$query ="SELECT * FROM `become_chef` where id ='$gid'";
		$req= $this->db->query($query);
			if($req->num_rows() > 0){
			
				$where = array('id' => $gid,'user_id'=>$userid);
				$data['updated_at'] = date('Y-m-d H:i:s');
				$isUpdate = $this->db->update('become_chef',$data,$where); // updata on become a chef
			
		        return array('id'=>$gid);//record update
			}
	}else{
				/*****register user as chef*******/
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				$isSaved = $this->db->insert('become_chef',$data);
				$lastinserted = $this->db->insert_id();
				
				return array('id'=>$lastinserted);//record insert
			}

	}

	/******Get kitchen gallery********/
	// function getKitchengallery($chefid){
	// 		$returnData = array();
	// 		$req="SELECT * FROM kitchen_gallery WHERE chef_id = '$chefid'";
	//          //print_r($req);die;
	// 		$query = $this->db->query($req);
	// 		if($query->num_rows() >0){
	// 			$returnData = $query->result();
	// 			foreach($returnData as $val){

	// 			if(empty($val->image1)){
	// 			$val->image1 = '';
	// 			} else {
	// 			$val->image1 = base_url().'assets/kitchengallery/'.$val->image1;
	// 			}

	// 			if(empty($val->image2)){
	// 			$val->image2 = '';
	// 			} else {
	// 			$val->image2 = base_url().'assets/kitchengallery/'.$val->image2;
	// 			}
	// 			if(empty($val->image3)){
	// 			$val->image3 = '';
	// 			} else {
	// 			$val->image3 = base_url().'assets/kitchengallery/'.$val->image3;
	// 			}
	// 			if(empty($val->image4)){
	// 			$val->image4 = '';
	// 			} else {
	// 			$val->image4 = base_url().'assets/kitchengallery/'.$val->image4;
	// 			}
	// 			if(empty($val->image5)){
	// 			$val->image5 = '';
	// 			} else {
	// 			$val->image5 = base_url().'assets/kitchengallery/'.$val->image5;
	// 			}
	// 			if(empty($val->image6)){
	// 			$val->image6 = '';
	// 			} else {
	// 			$val->image6 = base_url().'assets/kitchengallery/'.$val->image6;
	// 			}
	// 			if(empty($val->image7)){
	// 			$val->image7 = '';
	// 			} else {
	// 			$val->image7 = base_url().'assets/kitchengallery/'.$val->image7;
	// 			}
	// 			if(empty($val->image8)){
	// 			$val->image8 = '';
	// 			} else {
	// 			$val->image8 = base_url().'assets/kitchengallery/'.$val->image8;
	// 			}
	// 			if(empty($val->image9)){
	// 			$val->image9 = '';
	// 			} else {
	// 			$val->image9 = base_url().'assets/kitchengallery/'.$val->image9;
	// 			}
	// 			if(empty($val->image10)){
	// 			$val->image10 = '';
	// 			} else {
	// 			$val->image10 = base_url().'assets/kitchengallery/'.$val->image10;
	// 			}
	// 			if(empty($val->image11)){
	// 			$val->image11 = '';
	// 			} else {
	// 			$val->image11 = base_url().'assets/kitchengallery/'.$val->image11;
	// 			}
	// 			if(empty($val->image12)){
	// 			$val->image12 = '';
	// 			} else {
	// 			$val->image12 = base_url().'assets/kitchengallery/'.$val->image12;
	// 			}
	// 			if(empty($val->image13)){
	// 			$val->image13 = '';
	// 			} else {
	// 			$val->image13 = base_url().'assets/kitchengallery/'.$val->image13;
	// 			}
	// 			if(empty($val->image14)){
	// 			$val->image14 = '';
	// 			} else {
	// 			$val->image14 = base_url().'assets/kitchengallery/'.$val->image14;
	// 			}
	// 			if(empty($val->image15)){
	// 			$val->image15 = '';
	// 			} else {
	// 			$val->image15 = base_url().'assets/kitchengallery/'.$val->image15;
	// 			}
	// 			if(empty($val->image16)){
	// 			$val->image16 = '';
	// 			} else {
	// 			$val->image16 = base_url().'assets/kitchengallery/'.$val->image16;
	// 			}
	// 			if(empty($val->image17)){
	// 			$val->image17 = '';
	// 			} else {
	// 			$val->image17 = base_url().'assets/kitchengallery/'.$val->image17;
	// 			}
	// 			if(empty($val->image18)){
	// 			$val->image18 = '';
	// 			} else {
	// 			$val->image18 = base_url().'assets/kitchengallery/'.$val->image18;
	// 			}
	// 			if(empty($val->image19)){
	// 			$val->image19 = '';
	// 			} else {
	// 			$val->image19 = base_url().'assets/kitchengallery/'.$val->image19;
	// 			}if(empty($val->image20)){
	// 			$val->image20 = '';
	// 			} else {
	// 			$val->image20 = base_url().'assets/kitchengallery/'.$val->image20;
	// 			}

	// 			}
	// 			return $returnData;
	// 		}else{
	// 			return FALSE;
	// 		}
	// 	}

	function getKitchengallery($chefid){
			$returnData = array();
			$req="SELECT * FROM become_chef WHERE  user_id = '$chefid'";
	         //print_r($req);die;
			$query = $this->db->query($req);
			if($query->num_rows() >0){
				$returnData = $query->result();
				foreach($returnData as $val){

				if(empty($val->image1)){
				$val->image1 = '';
				} else {
				$val->image1 = base_url().'assets/kitchengallery/'.$val->image1;
				}

				if(empty($val->image2)){
				$val->image2 = '';
				} else {
				$val->image2 = base_url().'assets/kitchengallery/'.$val->image2;
				}
				if(empty($val->image3)){
				$val->image3 = '';
				} else {
				$val->image3 = base_url().'assets/kitchengallery/'.$val->image3;
				}
				if(empty($val->image4)){
				$val->image4 = '';
				} else {
				$val->image4 = base_url().'assets/kitchengallery/'.$val->image4;
				}
				if(empty($val->image5)){
				$val->image5 = '';
				} else {
				$val->image5 = base_url().'assets/kitchengallery/'.$val->image5;
				}
				if(empty($val->image6)){
				$val->image6 = '';
				} else {
				$val->image6 = base_url().'assets/kitchengallery/'.$val->image6;
				}
				if(empty($val->image7)){
				$val->image7 = '';
				} else {
				$val->image7 = base_url().'assets/kitchengallery/'.$val->image7;
				}
				if(empty($val->image8)){
				$val->image8 = '';
				} else {
				$val->image8 = base_url().'assets/kitchengallery/'.$val->image8;
				}
				if(empty($val->image9)){
				$val->image9 = '';
				} else {
				$val->image9 = base_url().'assets/kitchengallery/'.$val->image9;
				}
				if(empty($val->image10)){
				$val->image10 = '';
				} else {
				$val->image10 = base_url().'assets/kitchengallery/'.$val->image10;
				}
				if(empty($val->image11)){
				$val->image11 = '';
				} else {
				$val->image11 = base_url().'assets/kitchengallery/'.$val->image11;
				}
				if(empty($val->image12)){
				$val->image12 = '';
				} else {
				$val->image12 = base_url().'assets/kitchengallery/'.$val->image12;
				}
				if(empty($val->image13)){
				$val->image13 = '';
				} else {
				$val->image13 = base_url().'assets/kitchengallery/'.$val->image13;
				}
				if(empty($val->image14)){
				$val->image14 = '';
				} else {
				$val->image14 = base_url().'assets/kitchengallery/'.$val->image14;
				}
				if(empty($val->image15)){
				$val->image15 = '';
				} else {
				$val->image15 = base_url().'assets/kitchengallery/'.$val->image15;
				}
				if(empty($val->image16)){
				$val->image16 = '';
				} else {
				$val->image16 = base_url().'assets/kitchengallery/'.$val->image16;
				}
				if(empty($val->image17)){
				$val->image17 = '';
				} else {
				$val->image17 = base_url().'assets/kitchengallery/'.$val->image17;
				}
				if(empty($val->image18)){
				$val->image18 = '';
				} else {
				$val->image18 = base_url().'assets/kitchengallery/'.$val->image18;
				}
				if(empty($val->image19)){
				$val->image19 = '';
				} else {
				$val->image19 = base_url().'assets/kitchengallery/'.$val->image19;
				}if(empty($val->image20)){
				$val->image20 = '';
				} else {
				$val->image20 = base_url().'assets/kitchengallery/'.$val->image20;
				}

				}
				return $returnData;
			}else{
				return FALSE;
			}
		}

		/*********Delete Kitchen Gallery*********/
		// function DeleteKitchenGallery($data){
		// 	$kid = $data['id'];
		// 	$chef = $data['chef_id'];

		// $where = array('id' => $kid,'chef_id'=>$chef);
		// $isUpdate = $this->db->update('kitchen_gallery',$data,$where); // updata on user profile
		// if($isUpdate){
		
		// return array('id'=>$kid);//user profile updated successfully
		// }else{
		// 	 return FALSE;
		// }

		// }

		function DeleteKitchenGallery($data){
			$kid = $data['id'];
			$chef = $data['user_id'];

		$where = array('id' => $kid,'user_id'=>$chef);
		$isUpdate = $this->db->update('become_chef',$data,$where); // updata on user profile
		if($isUpdate){
		
		return array('id'=>$kid);//user profile updated successfully
		}else{
			 return FALSE;
		}

		}

		// function getprice(){

		// 	 $returnData = array();
		// 	$req = "SELECT * FROM  `menus`";
			
		// 	$query = $this->db->query($req);
		// 	if($query->num_rows() >0){
		// 	$returnData = $query->result();
		//  		}
	 	
		// return $returnData;

		// }

		/*******Store user lat long**********/
		function userLatLong($data,$user_id){

		$where = array('id'=>$user_id);
		$isUpdate = $this->db->update('register',$data,$where); // updata on user profile
		if($isUpdate){
		
		return array('id'=>$user_id);//user profile updated successfully
		}else{
			 return FALSE;
		}


		}

		function getfave(){
			$returnData = array();
			$req = "SELECT * FROM  `favourite_chef`";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;
		}
		/********Insert favouriot******/
		function favouriteChef($data){
			if(!empty($data)){
				$isSaved = $this->db->insert('favourite_chef',$data);
				$lastinserted = $this->db->insert_id();
				
				return array('id'=>$lastinserted);
			}else{
				return FALSE;
			}
		}

		/***********Delete chef***********/
		function deletefavouriteChef($data){

			if(!empty($data)){
				$userid = $data['user_id'];
				$chefid = $data['chef_id'];
				$tablename = 'favourite_chef';
				$where = array('user_id'=>$userid ,'chef_id'=>$chefid);
				$this->db->where($where);
	          	$deleteData =  $this->db->delete($tablename);
	          	return TRUE;
			}else{
				return FALSE;
			}
			
		}

		/**********Get Chef like or dislike**********/
		function wishlistChef($userid,$chefid){

			$returnData = array();
			$req = "SELECT * FROM  `favourite_chef` where user_id = '$userid' and chef_id='$chefid' ";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;

		}
		/*function wishlistChef($userid){

			$returnData = array();
			$req = "SELECT * FROM  `favourite_chef` where user_id = '$userid' ";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;

		}
*/
		function getMenusSubmenus($userid){
			$returnData = array();
			$req = "SELECT * FROM `menus` where user_id = '$userid' ";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;
		}

		/*function MenusSubmenus($userid,$menuid){
			
			$returnData = array();
			// $req="SELECT * FROM menus LEFT JOIN sub_menus ON menus.id = sub_menus.menu_id WHERE menus.id = '$menuid' and menus.user_id = sub_menus.user_id and menus.user_id = '$userid'";
			$req="SELECT * FROM menus LEFT JOIN sub_menus ON menus.id = sub_menus.menu_id WHERE menus.id in ($menuid) and menus.user_id = sub_menus.user_id and menus.user_id = '$userid'";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
			foreach($returnData as $val){
				if($val->menu_image !=''){
					$val->menu_image = base_url().'assets/menuImgae/'.$val->menu_image;
				}else{
					$val->menu_image = '';
				}
				if($val->dish_image !=''){
					$val->dish_image = base_url().'assets/submenuImage/'.$val->dish_image;
				}else{
					$val->dish_image = '';
				}
			}
		 		}
	 		return $returnData;

		}*/
		function MenusSubmenus($userid,$menuid){
			
			$returnData = array();
			// $req="SELECT * FROM menus LEFT JOIN sub_menus ON menus.id = sub_menus.menu_id WHERE menus.id = '$menuid' and menus.user_id = sub_menus.user_id and menus.user_id = '$userid'";
			$req="SELECT * FROM sub_menus where user_id ='$userid' and menu_id  ='$menuid' ";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
			foreach($returnData as $val){
				/*if($val->menu_image !=''){
					$val->menu_image = base_url().'assets/menuImgae/'.$val->menu_image;
				}else{
					$val->menu_image = '';
				}*/
				if($val->dish_image !=''){
					$val->dish_image = base_url().'assets/submenuImage/'.$val->dish_image;
				}else{
					$val->dish_image = '';
				}
			}
		 		}
	 		return $returnData;

		}

		function ChefmenuList($chefid){


			$returnData = array();
			$req="SELECT * FROM menus where user_id ='$chefid' ";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
			foreach($returnData as $val){
				/*if($val->menu_image !=''){
					$val->menu_image = base_url().'assets/menuImgae/'.$val->menu_image;
				}else{
					$val->menu_image = '';
				}*/
				if($val->menu_image !=''){
					$val->menu_image = base_url().'assets/menuImgae/'.$val->menu_image;
				}else{
					$val->menu_image = '';
				}
			}
		 		}
	 		return $returnData;

		}

		/********Get favroite chef*******/
		function getfavroite($user_id){
			$returnData = array();
			$req = "SELECT * FROM `favourite_chef` where user_id = '$user_id' ";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;
		}

		function getchefprofile($chefids){
			$returnData = array();
			if(!empty($chefids)){
				// $req="SELECT * FROM become_chef where id in ($chefids) ";
				$req = "SELECT *FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($chefids) ";
			//print_r($req);die;
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
			foreach($returnData as $val){
				if($val->facebook_login == 'yes' && $val->facebook_login !=''){
					if($val->profile_pic !=''){
					$val->profile_pic = $val->profile_pic;
					}else{
						$val->profile_pic = '';
					}

				}else{
					if($val->profile_pic !=''){
					$val->profile_pic = base_url().'assets/chefprofile/'.$val->profile_pic;
					}else{
						$val->profile_pic = '';
					}
				}
				

				if(empty($val->image1)){
				$val->image1 = '';
				} else {
				$val->image1 = base_url().'assets/kitchengallery/'.$val->image1;
				}

				if(empty($val->image2)){
				$val->image2 = '';
				} else {
				$val->image2 = base_url().'assets/kitchengallery/'.$val->image2;
				}
				if(empty($val->image3)){
				$val->image3 = '';
				} else {
				$val->image3 = base_url().'assets/kitchengallery/'.$val->image3;
				}
				if(empty($val->image4)){
				$val->image4 = '';
				} else {
				$val->image4 = base_url().'assets/kitchengallery/'.$val->image4;
				}
				if(empty($val->image5)){
				$val->image5 = '';
				} else {
				$val->image5 = base_url().'assets/kitchengallery/'.$val->image5;
				}
				if(empty($val->image6)){
				$val->image6 = '';
				} else {
				$val->image6 = base_url().'assets/kitchengallery/'.$val->image6;
				}
				if(empty($val->image7)){
				$val->image7 = '';
				} else {
				$val->image7 = base_url().'assets/kitchengallery/'.$val->image7;
				}
				if(empty($val->image8)){
				$val->image8 = '';
				} else {
				$val->image8 = base_url().'assets/kitchengallery/'.$val->image8;
				}
				if(empty($val->image9)){
				$val->image9 = '';
				} else {
				$val->image9 = base_url().'assets/kitchengallery/'.$val->image9;
				}
				if(empty($val->image10)){
				$val->image10 = '';
				} else {
				$val->image10 = base_url().'assets/kitchengallery/'.$val->image10;
				}
				if(empty($val->image11)){
				$val->image11 = '';
				} else {
				$val->image11 = base_url().'assets/kitchengallery/'.$val->image11;
				}
				if(empty($val->image12)){
				$val->image12 = '';
				} else {
				$val->image12 = base_url().'assets/kitchengallery/'.$val->image12;
				}
				if(empty($val->image13)){
				$val->image13 = '';
				} else {
				$val->image13 = base_url().'assets/kitchengallery/'.$val->image13;
				}
				if(empty($val->image14)){
				$val->image14 = '';
				} else {
				$val->image14 = base_url().'assets/kitchengallery/'.$val->image14;
				}
				if(empty($val->image15)){
				$val->image15 = '';
				} else {
				$val->image15 = base_url().'assets/kitchengallery/'.$val->image15;
				}
				if(empty($val->image16)){
				$val->image16 = '';
				} else {
				$val->image16 = base_url().'assets/kitchengallery/'.$val->image16;
				}
				if(empty($val->image17)){
				$val->image17 = '';
				} else {
				$val->image17 = base_url().'assets/kitchengallery/'.$val->image17;
				}
				if(empty($val->image18)){
				$val->image18 = '';
				} else {
				$val->image18 = base_url().'assets/kitchengallery/'.$val->image18;
				}
				if(empty($val->image19)){
				$val->image19 = '';
				} else {
				$val->image19 = base_url().'assets/kitchengallery/'.$val->image19;
				}if(empty($val->image20)){
				$val->image20 = '';
				} else {
				$val->image20 = base_url().'assets/kitchengallery/'.$val->image20;
				}

$val->allimage = $val->image1.'|'.$val->image2.'|'.$val->image3.'|'.$val->image4.'|'.$val->image5.'|'.$val->image6.'|'.$val->image7.'|'.$val->image8.'|'.$val->image9.'|'.$val->image10.'|'.$val->image11.'|'.$val->image12.'|'.$val->image13.'|'.$val->image14.'|'.$val->image15.'|'.$val->image16.'|'.$val->image17.'|'.$val->image18.'|'.$val->image19.'|'.$val->image20;


		$cusn = explode(",", $val->cuisine);
		    $cusn = array_values(array_unique(array_filter($cusn),SORT_REGULAR ));
		    if(!empty($cusn)){
		    	$cusns = implode(",", $cusn);
		    	$val->cuisine = $cusns;
		    }else{
		    	$val->cuisine='';
		    }
			}
		 		}
			}
			
			return $returnData;

		}


		function getfavechef($chefids){
			$returnData = array();
			$req = "SELECT * FROM  `favourite_chef` where chef_id in ($chefids) ";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;
		}

		function getprice(){
			$returnData = array();
			$req = "SELECT t.* FROM menus t JOIN ( SELECT *, MIN(price_per_person ) minVal FROM menus GROUP BY user_id ) t2 ON t.price_per_person = t2.minVal AND t.user_id = t2.user_id and t.user_id";
			//print_r($req);die;
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;
		}

		/*********Book Now*********/
		function bookNow($data){
		$booknow_id = $data['booknow_id'];
		$booking_id = $data['booking_id'];

		//check user exists in beacomechef table
		$query ="SELECT *  from booknow where booknow_id ='$booknow_id' and booking_id = '$booking_id'";
		    $req= $this->db->query($query);
			if($req->num_rows() > 0){
				/******Update profile of chef******/
				$where = array('booknow_id' => $booknow_id,'booking_id'=>$booking_id);
				$isUpdate = $this->db->update('booknow',$data,$where); // updata on become a chef
				
		        return array('booknow_id' => $booknow_id,'booking_id'=>$booking_id);//record update
			}else{
				/*****register user as chef*******/
				$isSaved = $this->db->insert('booknow',$data);
				$lastinserted = $this->db->insert_id();
				/******Get booking id ******/
				$query1 ="SELECT *  from booknow where booknow_id ='$lastinserted'";
				$req1= $this->db->query($query1);
				if($req1->num_rows() > 0){
					$bookingdetails = $req1->result();
				//print_r($chefdetails);
				$bookingkey = $bookingdetails[0]->booking_id;
				}
				return array('booknow_id' => $lastinserted,'booking_id'=>$bookingkey );//record update
			}

	}

	/*********Get Cuisine********/
	function getCuisine(){
		$returnData = array();
			$req = "SELECT * FROM  `menus` ";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;
	}


	function checktoken($authid){
		$returnData = array();
			$req = "SELECT * FROM  `register` where fb_auth_id='$authid' and is_verify ='yes'";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;
	}

	function  getdatafb($sid){
		$returnData = array();
			$req = "SELECT * FROM  `register` where id='$sid' and is_verify ='yes'";
			$query = $this->db->query($req);
			if($query->num_rows() >0){
			$returnData = $query->result();
		 		}
	 		return $returnData;
	}

	function updateInstagramtoken($userid,$data){
		$where = array('id'=>$userid);
		$isUpdate = $this->db->update('register',$data,$where); // updata on user profile
		if($isUpdate){
		
		return array('id'=>$userid);//user profile updated successfully
		}else{
			 return FALSE;
		}
	}
	}
?>
