<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_admin extends CI_Model{
    
    function __construct() {
		parent::__construct();
    }

    function isAdminLoggedIn($value,$value1){
        $query = "SELECT * FROM  `admin_login` WHERE  `username` =  '$value' AND  `password` =  '$value1'";
        //print_r($query);die;
		$query = $this->db->query($query);

        if($query->num_rows()){
            $res = $query->row();
            //set admin session
            $userdata = array('admin_id'=>$res->admin_id,
				'name'=>$res->name,
				'username'=>$res->username,
				'email'=>$res->email,
                'logged_in_back' => true
			);
            $this->session->set_userdata($userdata);
			return TRUE;
        } else {
            return FALSE;   
        }
	}

    /*****Privacy Policy by language*******/
    function getpolicy($sitelang,$userid){
        if($sitelang == ''){
            $sitelang ='english';
        }
       $query = "SELECT * FROM  `privacy_policy` WHERE status='1' and language ='$sitelang' and user_id='$userid' LIMIT 1";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }

    /*******Save Policy*******/
	function savePolicy($updateData,$id,$language){
        $where = @$this->db->where(array('id'=> $id,'language'=>$language));
        $updateData = @$this->db->update('privacy_policy',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }
    /******Terms & condition policy*****/
    function gettermsconditions($sitelang,$userid){
        if($sitelang == ''){
            $sitelang ='english';
        }
       $query = "SELECT * FROM  `terms_condition` WHERE status='1' and language ='$sitelang' and user_id='$userid' LIMIT 1";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }
    /*****Save terms & condition******/

    function saveTerms($updateData,$id,$language){
        $where = @$this->db->where(array('id'=> $id,'language'=>$language));
        $updateData = @$this->db->update('terms_condition',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    /*******Get all jobs******/
	
    function getalljobs($sitelang,$userid){
        if($sitelang == ''){
            $sitelang ='english';
        }
       $query = "SELECT * FROM  `jobs` WHERE status='1' and language ='$sitelang' and user_id='$userid' ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }

    /******Add new jobs*****/
    function saveJobs($insertData){
        if(!empty($insertData)){
            $this->db->insert('jobs',$insertData);
            return TRUE;
        }else{
            return FALSE;
        }
    }

    /****get a job**/
    function getjobs($id){
        $query = "SELECT * FROM  `jobs` WHERE status='1' and id ='$id' ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /******Save updated jobs*****/
    function saveupdatejobs($updateData,$id,$language){
        $where = @$this->db->where(array('id'=> $id,'language'=>$language));
        $updateData = @$this->db->update('jobs',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }
    /*****Delete jobs*****/
    function deletejobs($id){
        if (!empty($id)):
     $where = @$this->db->where('id',$id);
            $this->db->where($where);
            $this->db->delete('jobs');
        else: return "Invalid Input Provided";
        endif;
    }

    /*******Contact details*******/

    function getcontact($language,$userid){
         if($language == ''){
            $language ='english';
        }
       $query = "SELECT * FROM  `contact` WHERE status='1' and language ='$language' and user_id='$userid' ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /*****save Contact*****/

    function saveContact($updateData,$id,$language){
        $where = @$this->db->where(array('id'=> $id,'language'=>$language));
        $updateData = @$this->db->update('contact',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

     /*******Get all Users list******/
    
    function getallusers(){
       
       $query = "SELECT * FROM  `register` order by created_at desc ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }
    /********Get chef users*******/

    function getchefuser($id){
        $query = "SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
         become_chef.user_id = $id and register.is_chef='1'";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }

    /******Verify chef by admin*******/
    function Verfiychef($updateData,$userid){
        $where = @$this->db->where(array('id'=> $userid));
        $updateData = @$this->db->update('register',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    /********Get users*******/

    function getusers($id){
        $query = "SELECT * FROM register where id = '$id' ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }
    /******Verfiy Usersby admin*******/
    function VerfiyUsers($updateData,$userid){
        $where = @$this->db->where(array('id'=> $userid));
        $updateData = @$this->db->update('register',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }
    /******get menus*****/

    function getallList(){
        // $query = "SELECT * FROM menus LEFT JOIN register on menus.user_id = register.id where menus.user_id = register.id";
        $query = "SELECT m.id as menu_id, m.user_id,m.menu_title,m.menu_image,m.service_type,m.price_per_person,m.currency,m.created_at, m.updated_at,m.status,r.id,r.first_name,r.last_name FROM menus m,register r WHERE r.id = m.user_id
        order by m.created_at desc";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /******** get sub menu ********/

    function getallsubmenu($id){
        $query = "SELECT * FROM `sub_menus` WHERE `menu_id` = '$id' order by created_at desc";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function getmenudata($id){
        $query = "SELECT * FROM `menus` WHERE `id` = '$id'";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    

    function getsubmenudata($id){
        $query = "SELECT * FROM `sub_menus` WHERE `id` = '$id'";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function Savemenu($updateData,$menuid){
        $where = @$this->db->where(array('id'=> $menuid));
        $updateData = @$this->db->update('menus',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    function SaveSubmenu($updateData,$menuid){
        $where = @$this->db->where(array('id'=> $menuid));
        $updateData = @$this->db->update('sub_menus',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    /********Delete menus********/

    function deleteMenus($menuid){
        if(!empty($menuid)){
            $tablename = 'menus';
            $where = array('id'=>$menuid);
            $this->db->where($where);
          $deleteData =  $this->db->delete($tablename);

          if($deleteData){
            $tablename1 = 'sub_menus';
            $where1 = array('menu_id'=>$menuid);
            $this->db->where($where1);
          $this->db->delete($tablename1);
          }

            return TRUE;
        }else{
            return FALSE;
        }

    }

    /*******Delete submenus********/
    function deletesubMenus($submenuid){
        if(!empty($submenuid)){
            $tablename = 'sub_menus';
            $where = array('id'=>$submenuid);
            $this->db->where($where);
            $this->db->delete($tablename);
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
