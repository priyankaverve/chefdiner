<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Facebook App details
| -------------------------------------------------------------------
|
| To get an facebook app details you have to be a registered developer
| at http://developer.facebook.com and create an app for your project.
|
|  facebook_app_id               string   Your facebook app ID.
|  facebook_app_secret           string   Your facebook app secret.
|  facebook_login_type           string   Set login type. (web, js, canvas)
|  facebook_login_redirect_url   string   URL tor redirect back to after login. Do not include domain.
|  facebook_logout_redirect_url  string   URL tor redirect back to after login. Do not include domain.
|  facebook_permissions          array    The permissions you need.
|  facebook_graph_version        string   Set Facebook Graph version to be used. Eg v2.6
|  facebook_auth_on_load         boolean  Set to TRUE to have the library to check for valid access token on every page load.
*/

// $config['facebook_app_id']              = '170747193450777';
// $config['facebook_app_secret']          = 'ba1a0959a8143499adb354569c93e0a2';
// $config['facebook_login_type']          = 'web';
// // $config['facebook_login_redirect_url']  = 'index.php/welcome/web_login';
// // $config['facebook_logout_redirect_url'] = 'index.php/welcome/logout';
// $config['facebook_login_redirect_url']  = 'index.php/example/web_login';
// $config['facebook_logout_redirect_url'] = 'index.php/example/logout';
// $config['facebook_permissions']         = array('public_profile', 'publish_actions', 'email');
// $config['facebook_graph_version']       = 'v2.6';
// $config['facebook_auth_on_load']        = TRUE;

$config['facebook_app_id']              = '174134536517062';
$config['facebook_app_secret']          = '91570c981dc18ecbb8f547f56dbacddd';
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'index.php/apicontroller/fb_login';
$config['facebook_logout_redirect_url'] = 'index.php/apicontroller/fb_logout';
// $config['facebook_login_redirect_url']  = 'example/web_login';
// $config['facebook_logout_redirect_url'] = 'example/logout';
$config['facebook_permissions']         = array('public_profile', 'publish_actions', 'email');
$config['facebook_graph_version']       = 'v2.6';
$config['facebook_auth_on_load']        = TRUE;

