<?php
$lang['welcome_message'] = 'Welcome to CodexWorld';
$lang['language']='English';

/*****Admin login form********/
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['lost_password'] = 'Lost Password?';
$lang['login']='Login';
$lang['lost_password_msg']='Enter your e-mail address below and we will send you instructions how to recover a password.';
$lang['backtologin']='Back to login';
$lang['rcover']='Recover';
$lang['emailadd']='E-mail address';

/*****Admin main header********/
$lang['welcome'] = 'Welcome';
$lang['setting'] = 'Settings';
$lang['logout'] = 'Logout';
$lang['myprofile'] = 'My Profile';
$lang['mytask']='My Task';
$lang['sitetitle']='Chef Diner';
$lang['image']='Image';

/******Side bar menu******/
$lang['dashboard'] ='Dashboard';
$lang['user']='Users';
$lang['userlist']='Users List';
/*****breadcrumb ******/
$lang['home']='Home';
$lang['pages']='Pages';
$lang['privacy']='Privacy Policy';
$lang['jobs']='Jobs';
$lang['contact']='Contact';
$lang['addjobs']='Add Jobs';
$lang['editjobs']='Edit Jobs';
/****all pages label and placeholder****/
$lang['title']='Title';
$lang['content']='Content';
$lang['save']='Save';
$lang['termscondition']='Terms & Conditions';

/*****Success and error msg*****/
$lang['policysuccss'] = 'Privacy Policy Saved Successfully';
$lang['error']='Something went wrong,please try again';
$lang['termssuccss'] = 'Terms & Conditions Saved Successfully';
$lang['Jobsuccss'] = 'Jobs Saved Successfully';
$lang['Jobdelete'] = 'Jobs Deleted Successfully';
$lang['Contactsuccss'] = 'Contacts Saved Successfully';

$lang['chefverifysuccess'] = 'Chef updated Successfully';
$lang['userverifysuccess'] = 'User updated Successfully';
$lang['menusuccess']='Saved menu successfully';
$lang['submenusuccess']='Saved sub menu successfully';
$lang['deletemenus']='Menu deleted successfully';
$lang['deletesubmenus']='Submenu deleted successfully';

/*******Table content for list*******/
$lang['serialno'] = 'S No.';
$lang['usersName'] = 'Name';
$lang['usersEmail'] = 'Email';
$lang['isuser'] = 'Is User ?';
$lang['ischef'] = 'Is Chef ?';
$lang['isuserverify'] = 'User Verification';
$lang['ischefverify'] = 'Chef Verification';
$lang['jobtitle'] = 'Job title';
$lang['description'] = 'Description';
$lang['action'] = 'Action';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['verifychef']='Verify Chef'; 
$lang['preferlanguage']='What language do you prefer ?';
$lang['nameque']='What is your name ?';
$lang['speaklanguage']='What languages do you speak ?';
$lang['phonenum']='What is your phone number ?';
$lang['oftencook']='How often do you want to cook ?';
$lang['palcelocated']='Where is your place located ?';
$lang['numberPeople']='How many people can you host for a dinner ? ';
$lang['kitchenTitle']='What will be the title of your Kitchen ?';
$lang['kitchenDes']='Describe your Kitchen';
$lang['servicetype']='Select your Service Type';
$lang['uploadpic']='Upload profile picture';
$lang['contactno']='Phone Number';
$lang['verifyuser']='Verify User';
/*******Menu section********/
$lang['menu'] = 'Menus';
$lang['menulist'] = 'Menus List';
$lang['chefname'] = 'Chef Name';
$lang['menutitle'] = 'Menu Title';
$lang['menuimage']='Menu Image';
$lang['priceperperson'] = 'Price Per Person';
$lang['currency'] = 'Currency';
$lang['submenu'] = 'Sub Menu';
$lang['addedat'] = 'Added At';
$lang['status'] = 'Status';
$lang['view']='View';

/*****Sub menu section*****/
$lang['submenulist'] = 'Sub Menu List';
$lang['dishname'] = 'Dish Name';
$lang['dishimage'] = 'Dish Image';
$lang['dishcategory'] = 'Dish Category';

$lang['active']='Active';
$lang['deactive']='Deactive';

/****Edit menu***/
$lang['editmenu'] = 'Edit menus';
$lang['service']='Service Type';

/******edit sub menu******/
$lang['editsubmenu'] = 'Edit Sub Menu';

/*******Cuisines*********/
$lang['cuisine1'] = 'Hong Kong Style';
$lang['cuisine2'] = 'Chinese';

?>