<?php
$lang['welcome_message'] = '欢迎来到CodexWorld';
$lang['language']='簡體中文';

/*****Admin login form********/
$lang['username'] = '用户名称';
$lang['password'] = '密码';
$lang['lost_password'] = '忘记密码？';
$lang['login']='登录';
$lang['lost_password_msg']='在下面输入您的电子邮件地址，我们将向您发送如何恢复密码的说明。';
$lang['backtologin']='回到登录';
$lang['rcover']='恢复';
// $lang['emailadd']='电子邮件地址';
$lang['emailadd']='电子邮件';
$lang['image']='图片';

/*****Admin main header********/
$lang['welcome'] = '欢迎';
$lang['setting'] = '设置';
$lang['logout'] = '登出';
$lang['myprofile'] = '我的简历';
$lang['mytask']='我的任务';
$lang['sitetitle']='厨师与食客';//chef diner

/******Side bar menu******/
$lang['dashboard'] ='儀表板';
$lang['user']='用户';
$lang['userlist']='用户列表';
/*****breadcrumb ******/
$lang['home']='家';
$lang['pages']='网页';
$lang['privacy']='隐私政策';
$lang['jobs']='工作';
$lang['addjobs']='添加作业';
$lang['editjobs']='编辑作业';
$lang['contact']='联系';

/****all pages label and placeholder****/
$lang['title']='标题';
$lang['content']='内容';
$lang['save']='保存';
$lang['termscondition']='条款和条件';

/*****Success and error msg*****/
$lang['policysuccss'] = '隐私政策成功保存';
$lang['error']='出现问题，请重试';
$lang['termssuccss'] = '条款和条件成功保存';
$lang['Jobsuccss'] = '工作成功保存';
$lang['Jobdelete'] = '工作成功删除';
$lang['Contactsuccss'] = '联系人已成功保存';
$lang['chefverifysuccess'] = '厨师成功更新';
$lang['userverifysuccess'] = '用户成功更新';
$lang['menusuccess']='成功保存菜单';
$lang['submenusuccess']='保存子菜单成功';
$lang['deletemenus']='菜单已成功删除';
$lang['deletesubmenus']='子菜单已成功删除';

/*******Table content for list*******/
$lang['serialno'] = 'S号';
$lang['usersName'] = '名称';
$lang['usersEmail'] = 'Email';
$lang['isuser'] = '是用户？';
$lang['ischef'] = '是厨师吗？';
$lang['isuserverify'] = '用户验证';
$lang['ischefverify'] = '厨师验证';
$lang['jobtitle'] = '工作职衔';
$lang['description'] = '描述';
$lang['action'] = '行动';
$lang['yes'] = '是';
$lang['no'] = '不是';
$lang['verifychef']='验证厨师';
$lang['preferlanguage']='你想用的语言是？';
$lang['nameque']='你的全名是？';
$lang['speaklanguage']='你能已什么语言与客人沟通？';
$lang['phonenum']='你的电话号码是？';
$lang['oftencook']='你多久要做饭？';
$lang['palcelocated']='你的地址在哪里？';
$lang['numberPeople']='有多少人可以主持晚宴？';
$lang['kitchenTitle']='你厨房的名字是？';
$lang['kitchenDes']='描述你的厨房';
$lang['servicetype']='选择您的服务类型';
$lang['uploadpic']='上传个人资料图片';

$lang['contactno']='电话号码';
$lang['verifyuser']='验证用户';
/*******Menu section********/
$lang['menu'] = '菜单';
$lang['menulist'] = '菜单列表';
$lang['chefname'] = '厨师名称';
$lang['menutitle'] = '菜单标题';
$lang['menuimage']='菜单图像';
$lang['priceperperson'] = '每人价格';
$lang['currency'] = '货币';
$lang['submenu'] = '子菜单';
$lang['addedat'] = '添加在';
$lang['status'] = '状态';
$lang['view']='視圖';

/*****Sub menu section*****/
$lang['submenulist'] = '子菜单列表';
$lang['dishname'] = '菜名';
$lang['dishimage'] = '菜图像';
$lang['dishcategory'] = '菜类别';

$lang['active']='活性';
$lang['deactive']='去活';

/****Edit menu***/
$lang['editmenu'] = '编辑菜单';
$lang['service']='服务类型';

/******edit sub menu******/
$lang['editsubmenu'] = '编辑子菜单';
/*******Cuisines*********/
$lang['cuisine1'] = '港式';
$lang['cuisine2'] = '中文';
?>